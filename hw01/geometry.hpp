#ifndef __GEOMETRY_HPP
#define __GEOMETRY_HPP

#include <iostream>
#include <cmath>
#include <sstream>
#define NUM_INPUTS (4)
#define MAX_NUM_INPUTS_PER_LINE (3)
#define EXIT_FLAG (123456)
#define PI (3.141592653589793238463)
using namespace std;

namespace geometry {
    double euclidean_distance(double, double, double, double);
    double haversine(double, double, double, double);
}

namespace convert {
    bool is_int(double);
    bool is_dec(double);
    bool is_negative(double);
    void print_err(string, string);
    double to_radians(double);

    double to_total_degrees(string, string);

}


#endif