#include "geometry.hpp"

namespace geometry { // You should always use a namespace for your modules, to not pollute the global namespace
    double euclidean_distance(double x1, double y1, double x2, double y2) {
        return sqrt(pow(x2 - x1, 2.0) + pow(y2 - y1, 2.0));
    }

    double haversine(double lat1, double lon1, double lat2, double lon2) {
        // colatitudes a and b of each point: can be computed as 90−lat
        double a = 90 - lat1;
        double b = 90 - lat2;
        // the difference in longitude C between the two points
        double C = lon2 - lon1;
        // the size c of the angle between the two points with the center of the Earth as the vertex
        // c = cos^{−1} (cos(a)cos(b) + sin(a)sin(b)cos(C))
        double c = acos(cos(convert::to_radians(a)) * cos(convert::to_radians(b))
                        + sin(convert::to_radians(a)) * sin(convert::to_radians(b)) * cos(convert::to_radians(C)));

        const double R = 3959; // radius of Earth in miles

        // converting that angle to a distance by multiplying by the radius of the Earth, which is 3959 miles
        return c * R;
    }

}

namespace convert {
    bool is_int(double num) {
        if (num == (int) num) {
            return true;
        }
        return false;
    }

    bool is_dec(double num) {
        return true;
    }

    bool is_negative(double num) {
        return (num < 0);
    }

    void print_err(string str, string coord_type) {
        cout << "\t At (" << coord_type << "): " << str << endl;
    }

    double to_radians(double degrees) {
        return 2 * PI * degrees / 360;
    }

    // "there are three possible formats for each line:
        // 1) decimal degrees;
        // 2) integer degrees and decimal minutes; and
        // 3) integer degrees and minutes and decimal seconds."
    // @param coord_type: "lat" or "lon"
    double to_total_degrees(string str, string coord_type) {
        double inputs[MAX_NUM_INPUTS_PER_LINE];
        istringstream sin;
        sin.str(str);
        int num_inputs = 0;
        double temp;
        while (sin >> temp) {
            inputs[num_inputs] = temp;
            num_inputs++;
        }

        double deg = inputs[0];
        double min = inputs[1];
        double sec = inputs[2];
        double total_degrees;

        // Validate inputs are correct format
        if (num_inputs == 0) {
            cout << "FAIL: Invalid line of input" << endl;
            print_err(str, coord_type);
            return EXIT_FLAG;
        }
        // 1) decimal degrees
        else if (num_inputs == 1) {
            if (!(is_dec(deg))) {
                cout << "FAIL: Requires decimal degrees" << endl;
                print_err(str, coord_type);
                return EXIT_FLAG;
            }
            total_degrees = deg;
        }
        // 2) integer degrees and decimal minutes
        else if (num_inputs == 2) {
            if (!(is_int(deg) && is_dec(min))) {
                cout << "FAIL: Requires integer degrees and decimal minutes" << endl;
                print_err(str, coord_type);
                return EXIT_FLAG;
            }
            if (is_negative(min)) {
                cout << "FAIL: Only degrees can be negative" << endl;
                print_err(str, coord_type);
                return EXIT_FLAG;
            }
            if (min >= 60.0) {
                cout << "FAIL: Minutes must be in range 0 <= x < 60" << endl;
                print_err(str, coord_type);
                return EXIT_FLAG;
            }
            if (is_negative(deg)) {
                total_degrees = -(-deg + min / 60.0);
            }
            else {
                total_degrees = deg + min / 60.0;
            }

        }
        // 3) integer degrees and minutes and decimal seconds
        else if (num_inputs == 3) {
            if (!(is_int(deg) && is_int(min) && is_dec(sec))) {
                cout << "FAIL: Requires integer degrees, integer minutes, and decimal seconds" << endl;
                print_err(str, coord_type);
                return EXIT_FLAG;
            }
            if (is_negative(min) || is_negative(sec)) {
                cout << "FAIL: Only degrees can be negative" << endl;
                print_err(str, coord_type);
                return EXIT_FLAG;
            }
            if (min >= 60.0 || sec >= 60.0) {
                cout << "FAIL: Minutes and seconds must be in range 0 <= x < 60" << endl;
                print_err(str, coord_type);
                return EXIT_FLAG;
            }
            if (is_negative(deg)) {
                total_degrees = -(-deg + min / 60.0 + sec / (60.0 * 60.0));
            }
            else {
                total_degrees = deg + min / 60.0 + sec / (60.0 * 60.0);
            }
        }

        // Validate degree ranges
        // "The values given are within the range +/- 90 degrees for latitude and +/- 180 degrees for longitude"
        if (coord_type == "lat") {
            if (total_degrees < -90.0 || total_degrees > 90.0) {
                cout << "FAIL: Latitude mast be in range -90 <= x <= 90 degrees" << endl;
                print_err(str, coord_type);
                return EXIT_FLAG;
            }
        }
        else if (coord_type == "lon") {
            if (total_degrees < -180.0 || total_degrees > 180.0) {
                cout << "FAIL: Longitude must be in range -180 <= x <= 180 degrees" << endl;
                print_err(str, coord_type);
                return EXIT_FLAG;
            }
        }

        return total_degrees;

    }
}
