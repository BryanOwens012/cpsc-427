// Bryan Owens

#include <iostream>
#include <string>
#include "geometry.hpp"
using namespace std;

int main()
{
    try {
        // Read inputs
        string inputs[NUM_INPUTS];
        for (int i = 0; i < NUM_INPUTS; i++) {
            if (!(getline(cin, inputs[i]))) {
                cout << "FAIL: Requires 4 lines of input" << endl;
                return 0;
            }
        }
        double lat1, lon1, lat2, lon2;
        lat1 = convert::to_total_degrees(inputs[0], "lat");
        lon1 = convert::to_total_degrees(inputs[1], "lon");
        lat2 = convert::to_total_degrees(inputs[2], "lat");
        lon2 = convert::to_total_degrees(inputs[3], "lon");
        if (lat1 == EXIT_FLAG || lon1 == EXIT_FLAG || lat2 == EXIT_FLAG || lon2 == EXIT_FLAG) {
            return 0;
        }
        cout << geometry::haversine(lat1, lon1, lat2, lon2) << endl;

    } catch (string e) {
        cout << "An exception occurred: " << e << endl;
    }
    return 0;
}