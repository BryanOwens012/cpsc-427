#ifndef __YAHTZEE_HPP__
#define __YAHTZEE_HPP__

#include "diceroll.hpp"
#include "rule.hpp"
#include "category.hpp"

#include <iostream>
#include <vector>
#include <unordered_map>
#include <memory>
using namespace std;

namespace cs427_527
{
    class YahtzeeGame;
    class Category;

    class Scoresheet
    {
    public:
        Scoresheet(const YahtzeeGame& g, vector<shared_ptr<Rule> > rs, vector<Category> cats);
        bool isTerminal() const;
        void output(std::ostream& os) const;
        vector<string> unusedCategories() const;
        Category& abbrev2Cat(string abbrev);

        const YahtzeeGame& game;
        vector<shared_ptr<Rule> > rules;
        vector<Category> categories;
        vector<Category> bonuses;
        unordered_map<string, int> abbrev_2_cat; // index
    };

    class YahtzeeGame
    {
    public:
        YahtzeeGame(vector<shared_ptr<Rule> > rs, vector<Category> cats);
        Scoresheet initialSheet() const;
        bool isTerminal(const Scoresheet& sheet) const;
        void scoreRoll(const DiceRoll& roll, string cat, Scoresheet& sheet) const;

        vector<int> dice;
        vector<shared_ptr<Rule> > rules;
        vector<Category> categories;
    };

    std::ostream& operator<<(std::ostream& os, const Scoresheet& sheet);

}

#endif