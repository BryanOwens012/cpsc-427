#ifndef __RULE_HPP__
#define __RULE_HPP__

#include "diceroll.hpp"
//#include "yahtzee.hpp"

#include <iostream>
#include <vector>
#include <unordered_map>
#include <memory>
using namespace std;

namespace cs427_527 {

    class Scoresheet;

    class Rule {
    public:
        virtual ~Rule() = default;
        virtual int getScore(const DiceRoll &roll) const = 0;
        virtual int getScore(const DiceRoll &roll, Scoresheet &sheet) const {
            return score;
        }
        Rule();

        Rule(const Rule& other);

        Rule& operator=(const Rule &other);

        string name;
        string abbrev;
        int score = 0;
    };

    class FixedScoreRule : public Rule {
    public:
        FixedScoreRule();
        virtual int getScore(const DiceRoll &roll) const override;
    };

    class SumScoreRule : public Rule {
    public:
        SumScoreRule();
        virtual int getScore(const DiceRoll &roll) const override;
    };

    class DiceOfN : public Rule {
        // Acies, Twos, Threes, Fours, Fives, Sixes
    public:
        DiceOfN(int n);
        virtual int getScore(const DiceRoll &roll) const override;

        int n;
    };

    class NOfAKind : public SumScoreRule {
        // E.g., 3 of a kind, 4 of a kind
    public:
        NOfAKind(int n);
        virtual int getScore(const DiceRoll &roll) const override;
        int n;
    };

    class FullHouse : public FixedScoreRule {
        // x1 of one number and x2 of another
    public:
        FullHouse(int x1 = 3, int x2 = 2, int score = 25);
        virtual int getScore(const DiceRoll &roll) const override;
        int x1, x2;
    };

    class Straight : public FixedScoreRule {
        // x sequential dice
    public:
        Straight(int x, int score = 30);
        virtual int getScore(const DiceRoll &roll) const override;
        int x;
    };

    class Yahtzee : public Rule {
        // All five dice the same
    public:
        virtual int getScore(const DiceRoll &roll) const override;
        Yahtzee(int score = 50);
    };

    class Chance : public SumScoreRule {
        // Any combination
    public:
        Chance();
    };

    class BonusRule : public Rule {
    public:
        BonusRule(int bonus = 35);
        virtual ~BonusRule() = default;
        virtual int getScore(const DiceRoll &roll) const override;
        virtual int getScore(const DiceRoll &roll, Scoresheet &sheet) const override {
            return bonus;
        }

        int bonus;
    };

    class UpperBonusRule : public BonusRule {
        // If a player scores a total of 63 or more points in the upper section,
        // a bonus of 35 is added to the upper section score.
    public:
        UpperBonusRule(vector<int> category_range, int threshhold, int own_cat_index, int bonus = 35);
        virtual int getScore(const DiceRoll &roll, Scoresheet &sheet) const override;

        int threshhold;
        int own_cat_index;
        vector<int> category_range;
    };

    // Modifications start here

    class TwoPair : public SumScoreRule {
    public:
        virtual int getScore(const DiceRoll &roll) const override;
    };

    // For both LowStraight and HighStraight
    class InclusionStraight: public Straight {
    public:
        InclusionStraight(int x, int inclusion = 1, int score = 25) : Straight(x, score) {
            this->x = x;
            this->inclusion = 1;
            this->score = score;
        };
        virtual int getScore(const DiceRoll &roll) const override;

        int inclusion;
    };

    class Yatzie : public NOfAKind {
    public:
        Yatzie(int n = 5, int plus = 30) : NOfAKind(n) {
            this->n = n;
            this->plus = plus;
        }
        virtual int getScore(const DiceRoll &roll) const override;

        int plus;
    };

    class Yortzee : public FixedScoreRule {
    public:
        Yortzee(int score = 15) {
            this->score = score;
        }
        virtual int getScore(const DiceRoll &roll) const override;
    };


    class Tier {
    public:
        Tier(int points, int start, int end) : points(points), start(start), end(end) {
        }

        int points, start, end;
    };

    class TieredUpperBonusRule : public BonusRule {
    public:
        TieredUpperBonusRule(vector<Tier> tiers, vector<int> category_range, int own_cat_index) :
                tiers(tiers), category_range(category_range), own_cat_index(own_cat_index) {
        }

        virtual int getScore(const DiceRoll &roll, Scoresheet &sheet) const override;

        vector<Tier> tiers;
        vector<int> category_range;
        int own_cat_index;
    };

    bool in_vect(const string& str, const vector<string>& vect);

    string find_diff(const vector<string>& vect1, const vector<string>& vect2);

    static vector<string> unused_categories;
    static int bonus_count = 0;

    class YortzeeBonusRule : public BonusRule {
    public:
        YortzeeBonusRule(int multiplier = 5) : multiplier(multiplier) {
        }

        virtual int getScore(const DiceRoll &roll, Scoresheet &sheet) const;

        int multiplier;
    };

}

#endif