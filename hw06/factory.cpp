#include "factory.hpp"

#include <memory>
using namespace std;

namespace cs427_527
{

    YahtzeeGame BasicYahtzeeFactory::makeGame() const {
        // Create rules
        vector<shared_ptr<Rule> > rules;
        for (int i = 1; i <= example_dice.NUM_SIDES; i++) {
            rules.push_back(make_shared<DiceOfN>(DiceOfN(i)));
        }
        vector<int> upper_bonus_indexes = {0, 1, 2, 3, 4, 5};
        rules.push_back(make_shared<UpperBonusRule>(UpperBonusRule(upper_bonus_indexes, 63, 8, 35)));
        for (int i = 3; i <= 4; i++) {
            rules.push_back(make_shared<NOfAKind>(NOfAKind(i)));
        }
//        vector<shared_ptr<Rule> > other_rules = { make_shared<UpperBonusRule>(UpperBonusRule()), make_shared<FullHouse>(FullHouse()),
//                                     make_shared<Straight>(Straight(4)), make_shared<Straight>(Straight(5)),
//                                     make_shared<Yahtzee>(Yahtzee()), make_shared<Chance>(Chance()) };
        vector<shared_ptr<Rule> > other_rules;
        other_rules.push_back(make_shared<FullHouse>(FullHouse()));
        other_rules.push_back(make_shared<Straight>(Straight(4, 30)));
        other_rules.push_back(make_shared<Straight>(Straight(5, 40)));
        other_rules.push_back(make_shared<Chance>(Chance()));
        other_rules.push_back(make_shared<Yahtzee>(Yahtzee(50)));
        rules.insert(rules.end(), std::begin(other_rules), std::end(other_rules));

        // Create categories
        vector<Category> categories = { Category("1", "Aces", rules[0]), Category("2", "Deuces", rules[1]),
                                        Category("3", "Treys", rules[2]), Category("4", "Fours", rules[3]),
                                        Category("5", "Fives", rules[4]), Category("6", "Sixes", rules[5]),
                                        Category("UB", "UPPER BONUS", rules[6], true), // is a bonus
                                        Category("3K", "Three of a Kind", rules[7]),
                                        Category("4K", "Four of a Kind", rules[8]),
                                        Category("FH", "Full House", rules[9]),
                                        Category("SS", "Small Straight", rules[10]),
                                        Category("LS", "Large Straight", rules[11]),
                                        Category("C", "Chance", rules[12]),
                                        Category("Y", "Yahtzee", rules[13]) };

        return YahtzeeGame(rules, categories);

    }

    YahtzeeGame MysteryYahtzeeFactory::makeGame() const {
// Create rules
        vector<shared_ptr<Rule> > rules;
        for (int i = 1; i <= example_dice.NUM_SIDES; i++) {
            rules.push_back(make_shared<DiceOfN>(DiceOfN(i)));
        }

        vector<int> upper_bonus_indexes = {0, 1, 2, 3, 4, 5};
        vector<Tier> upper_bonus_tiers = { Tier(15, 42, 62), Tier(35, 63, 83), Tier(55, 84, 104), Tier(75, 105, 105)};
        rules.push_back(make_shared<TieredUpperBonusRule>(TieredUpperBonusRule(upper_bonus_tiers, upper_bonus_indexes, 6)));

        rules.push_back(make_shared<TwoPair>(TwoPair()));
        for (int i = 3; i <= 4; i++) {
            rules.push_back(make_shared<NOfAKind>(NOfAKind(i)));
        }
//        vector<shared_ptr<Rule> > other_rules = { make_shared<UpperBonusRule>(UpperBonusRule()), make_shared<FullHouse>(FullHouse()),
//                                     make_shared<Straight>(Straight(4)), make_shared<Straight>(Straight(5)),
//                                     make_shared<Yahtzee>(Yahtzee()), make_shared<Chance>(Chance()) };
        vector<shared_ptr<Rule> > other_rules;
        other_rules.push_back(make_shared<FullHouse>(FullHouse()));
        other_rules.push_back(make_shared<InclusionStraight>(InclusionStraight(5, 1, 25)));
        other_rules.push_back(make_shared<InclusionStraight>(InclusionStraight(5, example_dice.NUM_SIDES, 30)));
        other_rules.push_back(make_shared<Yatzie>(Yatzie(5, 30)));
        other_rules.push_back(make_shared<Chance>(Chance()));
        other_rules.push_back(make_shared<Yortzee>(Yortzee(15)));
        other_rules.push_back(make_shared<YortzeeBonusRule>(YortzeeBonusRule(5)));
        rules.insert(rules.end(), std::begin(other_rules), std::end(other_rules));

        // Create categories
        vector<Category> categories = { Category("1", "Aces", rules[0]), Category("2", "Deuces", rules[1]),
                                        Category("3", "Treys", rules[2]), Category("4", "Fours", rules[3]),
                                        Category("5", "Fives", rules[4]), Category("6", "Sixes", rules[5]),
                                        Category("UB", "UPPER BONUS", rules[6], true), // is a bonus
                                        Category("2P", "Two Pair", rules[7]),
                                        Category("3X", "Three of a Kind", rules[8]),
                                        Category("4X", "Four of a Kind", rules[9]),
                                        Category("FH", "Full House", rules[10]),
                                        Category("LS", "Low Straight", rules[11]),
                                        Category("HS", "High Straight", rules[12]),
                                        Category("Y", "Yatzie", rules[13]),
                                        Category("C", "Chance", rules[14]),
                                        Category("Z", "Yortzee", rules[15]),
                                        Category("YB", "YORTZEE BONUS", rules[16], true)
        };

        return YahtzeeGame(rules, categories);
    }

//    shared_ptr<IYahtzeePlayer> AdaptivePlayerFactory::makeConservativePlayer() const
//    {
//        return make_shared<YahtzeePlayer>(make_shared<GreedyProgressCategoryStrategy>(),
//                                        make_shared<DynamicLimitRollStrategy>(4, 3));
//    }
//
//    shared_ptr<IYahtzeePlayer> AdaptivePlayerFactory::makeNormalPlayer() const
//    {
//        return make_shared<YahtzeePlayer>(make_shared<GreedyProgressCategoryStrategy>(),
//                                        make_shared<DynamicLimitRollStrategy>(5, 2));
//    }
//
//    shared_ptr<IYahtzeePlayer> AdaptivePlayerFactory::makeAggressivePlayer() const
//    {
//        return make_shared<YahtzeePlayer>(make_shared<GreedyProgressCategoryStrategy>(),
//                                        make_shared<DynamicLimitRollStrategy>(8, 1));
//    }


}
