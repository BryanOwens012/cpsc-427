#ifndef __CATEGORY_HPP__
#define __CATEGORY_HPP__

#include "yahtzee.hpp"
#include "rule.hpp"

#include <iostream>
#include <memory>
using namespace std;

namespace cs427_527 {

    class Category {
    public:
        Category(string abbrev, string full_name, shared_ptr<Rule> r, bool is_bonus = false);
        void update(int score);
        void set_to_unused();
        void set_to_used();
        void output_abbrev(std::ostream& os) const;
        void output_full(std::ostream& os) const;

        Category(const Category& other);
        Category& operator=(const Category& other);


        string abbrev;
        string full_name;
//        Scoresheet& scoresheet;
        shared_ptr<Rule> rule;
        bool used = false;
        int score = 0;
        bool is_bonus = false;
    };

    std::ostream& operator<<(std::ostream& os, const Category& cat);

}

#endif