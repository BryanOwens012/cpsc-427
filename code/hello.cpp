#include <iostream>
#include <string>
#include <sstream>
using namespace std;

int main()
{
    string s = "Hello world"; // not null-terminated
    cout << s << endl;
    // C++ strings are not — strictly speaking — arrays of characters
    // They contain an array of chars, but also extra info

    cout << s[0] << s[-1] << endl;
    // cout << s[s.length() -1] << endl;
    // C++ strings do error checking, while C strings do not (C strings just corrupt the adjacent memory)

    string name = "asdf"
    istringstream sin[name]; // string extraction
    string first;
    sin >> first;

}