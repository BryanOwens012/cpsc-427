#include <cmath>

#include "geometry.hpp"

namespace geometry { // You should always use a namespace for your modules, to not pollute the global namespace
    double euclidean_distance(double x1, double y1, double x2, double y2) {
        return sqrt(pow(x2 - x1, 2.0) + pow(y2 - y1, 2.0));
    }

    namespace nested {
        void doStuff() {

        }
    }
}

