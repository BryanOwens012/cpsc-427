#ifndef __MATRIX_HPP
#define __MATRIX_HPP
#define ALL (1)

#include <iostream>
#include <string>
using namespace std;

namespace cs427527
{
    template<class T>
    class Matrix
    {
    public:

        class iterator {
        public:
            iterator(string t, int pos, T** mat, int h, int w);
            iterator(string t, int pos, T** mat, int pos2);
            iterator(const iterator &other);
            iterator &operator=(const Matrix &other);
            T &operator*();
            const T &operator*() const;
            iterator &operator++();
            bool operator==(const iterator &rhs);
            bool operator!=(const iterator &rhs);
            string type;
            int curr_row, curr_col;
            T** mat;
            int h, w;
            int position;
        };

        class const_iterator: public Matrix {
        public:
            const_iterator();
            const_iterator(string t, int pos);
            const_iterator(string t, int pos, int pos2);
            const_iterator(const const_iterator& other);
            const_iterator& operator=(const Matrix& other);
            T& operator*();
            const T& operator*() const;
            const_iterator& operator++();
            bool operator==(const const_iterator& rhs);
            bool operator!=(const const_iterator& rhs);
            string type;
            int curr_row, curr_col;
            T** mat;
            int h, w;
        };

        class slice {
        public:
            slice();
            slice(string t, int pos, T** mat);
            slice(const slice& other);
            T& operator[](int i);
            const T& operator[](int i) const;
            iterator begin();
            iterator end();
            const_iterator begin() const;
            const_iterator end() const;
            string type;
            int position;
            T** mat;
            int h, w;
//            iterator curr_begin, curr_end;
        };

        class const_slice: public Matrix {
        public:
            const_slice();
            const_slice(string t, int pos);
            const_slice(const const_slice& other);
            T& operator[](int i);
            const T& operator[](int i) const;
            const_iterator begin() const;
            const_iterator end() const;
            string type;
            int position;
            T** mat;
            int h, w;
        };

        static void initMat(T** mat, int h, int w);
        void destroyMat(T** mat, int h, int w);
        Matrix();
        Matrix(int h, int w);
        Matrix(const Matrix& other);
        Matrix(Matrix&& other);
        Matrix& operator=(const Matrix& other);
        Matrix& operator=(Matrix&& other);
        int height();
        int height() const;
        int width();
        int width() const;
        T& at(int r, int c);
        const T& at(int r, int c) const;
        ~Matrix();
        slice operator[](int row);
        const_slice operator[](int row) const;
        slice column(int col);
        const_slice column(int col) const;

        T** mat;
        int h, w;
    };

    template<class T>
    void Matrix<T>::initMat(T** mat, int h, int w) {
        for (int i = 0; i < h; i++) {
            mat[i] = new T[w]();
        }
    }

    template<class T>
    void Matrix<T>::destroyMat(T** mat, int h, int w) {
        for (int i = 0; i < h; i++) {
            delete[] mat[i];
        }
        delete[] mat;
    }

    template<class T>
    Matrix<T>::Matrix() {
        this->h = 1;
        this->w = 1;
        mat = new T*[this->h];
        initMat(mat, h, w);
    }

    template<class T>
    Matrix<T>::Matrix(int h, int w) {
        this->h = h;
        this->w = w;
        mat = new T*[h];
        initMat(mat, h, w);
    }

    // Copy constructor
    template<class T>
    Matrix<T>::Matrix(const Matrix& other) {
        h = other.h;
        w = other.w;
        mat = new T*[h];
        initMat(mat, h, w);
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                T val = other.mat[i][j];
                mat[i][j] = val;
            }
        }
    }

    // Move constructor
    template<class T>
    Matrix<T>::Matrix(Matrix&& other) {
        h = other.h;
        w = other.w;
        mat = other.mat;
        // make other's mat point somewhere else
        // so it doesn't corrupt the new object when it gets cleaned up
        other.mat = nullptr;
    }

    // Copy assignment operator
    // https://www.geeksforgeeks.org/copy-constructor-vs-assignment-operator-in-c/
    template<class T>
    Matrix<T>& Matrix<T>::operator=(const Matrix& other) {
        if (this == &other) {
            Matrix<T>& th = *this;
            return th;
        }
        h = other.h;
        w = other.w;
        mat = new T*[h];
        initMat(mat, h, w);
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                T val = other.mat[i][j];
                mat[i][j] = val;
            }
        }
        Matrix<T>& th = *this;
        return th;
    }

    // Move assignment operator
    // https://docs.microsoft.com/en-us/cpp/cpp/move-constructors-and-move-assignment-operators-cpp?view=vs-2017
    template<class T>
    Matrix<T>& Matrix<T>::operator=(Matrix&& other) {
        if (this == &other) {
            Matrix<T>& th = *this;
            return th;
        }
        // destroyMat(mat, h, w);
        h = other->h;
        w = other->w;
        mat = other->mat;
        other->mat = nullptr;
        Matrix<T>& th = *this;
        return th;
    }

    template<class T>
    int Matrix<T>::height() {
        return h;
    }

    template<class T>
    int Matrix<T>::height() const {
        return h;
    }

    template<class T>
    int Matrix<T>::width() {
        return w;
    }

    template<class T>
    int Matrix<T>::width() const {
        return w;
    }

    template<class T>
    T& Matrix<T>::at(int r, int c) {
        if (r < 0 || r >= h) {
            throw std::out_of_range("r must be 0 <= r < h");
        }
        if (c < 0 || c >= w) {
            throw std::out_of_range("c must be 0 <= c < w");
        }
        T& ref = mat[r][c];
        return ref;
    }

    template<class T>
    const T& Matrix<T>::at(int r, int c) const {
        if (r < 0 || r >= h) {
            throw std::out_of_range("r must be 0 <= r < h");
        }
        if (c < 0 || c >= w) {
            throw std::out_of_range("c must be 0 <= c < w");
        }
        const T& ref = mat[r][c];
        return ref;
    }

    template<class T>
    Matrix<T>::~Matrix() {
        try {
            destroyMat(mat, h, w);
        } catch (...) {
        }
    }

    template<class T>
    typename Matrix<T>::slice Matrix<T>::operator[](int row) {
        if (row < 0 || row >= h) {
            throw std::out_of_range("row must be 0 <= row < h");
        }
        return slice("row", row, mat);
    }

    template<class T>
    typename Matrix<T>::const_slice Matrix<T>::operator[](int row) const {
        if (row < 0 || row >= h) {
            throw std::out_of_range("row must be 0 <= row < h");
        }
        return const_slice("row", row);
    }

    template<class T>
    typename Matrix<T>::slice Matrix<T>::column(int col) {
        if (col < 0 || col >= w) {
            throw std::out_of_range("col must be 0 <= col < w");
        }
        return slice("col", col);
    }

    template<class T>
    typename Matrix<T>::const_slice Matrix<T>::column(int col) const {
        if (col < 0 || col >= w) {
            throw std::out_of_range("col must be 0 <= col < w");
        }
        return const_slice("col", col);
    }

        template<class T>
        Matrix<T>::iterator::iterator(string t, int pos, T** mat, int h, int w) {
           type = t;
           this->h = h;
           this->w = w;
           this->mat = mat;
           if (type == "row") {
                curr_row = pos;
                curr_col = 0;
           } else {
               curr_row = 0;
               curr_col = pos;
           }
           cout << "iterator(" << t << ", " << pos << endl;
        }

        template<class T>
        Matrix<T>::iterator::iterator(string t, int pos, T** mat, int pos2) {
            type = t;
            this->position = pos;
            this->mat = mat;
            if (type == "row") {
                curr_row = pos;
                curr_col = pos2;
            } else {
                curr_row = pos2;
                curr_col = pos;
            }
        }

        template<class T>
        Matrix<T>::iterator::iterator(const iterator& other) {
            type = other.type;
            curr_row = other.curr_row;
            curr_col = other.curr_col;
        }

        template<class T>
        typename Matrix<T>::iterator& Matrix<T>::iterator::operator=(const Matrix& other) {
            if (this == &other) {
                Matrix<T>::iterator& th = *this;
                return th;
            }
            type = other.type;
            curr_row = other.curr_row;
            curr_col = other.curr_col;
            Matrix<T>::iterator& th = *this;
            return th;
        }

        template<class T>
        T& Matrix<T>::iterator::operator*() {
            T& ret = mat[curr_row][curr_col];
            cout << "mat[" << curr_row << "][" << curr_col << "]" << endl;
            cout << ret << endl;
            return ret;
        }

        template<class T>
        const T& Matrix<T>::iterator::operator*() const {
            const T& ret = mat[curr_row][curr_col];
            return ret;
        }

        template<class T>
        typename Matrix<T>::iterator& Matrix<T>::iterator::operator++() {
            if (type == "row") {
                if (curr_row < h - 1) {
                    curr_row++;
                }
            } else {
                if (curr_col < w - 1) {
                    curr_col++;
                }
            }
            Matrix<T>::iterator& th = *this;
            return th;
        }

        template<class T>
        bool Matrix<T>::iterator::operator==(const iterator& rhs) {
            return this == &rhs;
        }

        template<class T>
        bool Matrix<T>::iterator::operator!=(const iterator& rhs) {
            return !(operator==(rhs));
        }

        template<class T>
        Matrix<T>::const_iterator::const_iterator() {

        }

        template<class T>
        Matrix<T>::const_iterator::const_iterator(string t, int pos) {
            type = t;
            if (type == "row") {
                curr_row = pos;
                curr_col = 0;
            } else {
                curr_row = 0;
                curr_col = pos;
            }
        }

        template<class T>
        Matrix<T>::const_iterator::const_iterator(string t, int pos, int pos2) {
            type = t;
            if (type == "row") {
                curr_row = pos;
                curr_col = pos2;
            } else {
                curr_row = pos2;
                curr_col = pos;
            }
        }

        template<class T>
        Matrix<T>::const_iterator::const_iterator(const const_iterator& other) {
            type = other.type;
            curr_row = other.curr_row;
            curr_col = other.curr_col;
        }

        template<class T>
        typename Matrix<T>::const_iterator& Matrix<T>::const_iterator::operator=(const Matrix& other) {
            if (this == &other) {
                Matrix<T>::iterator& th = *this;
                return th;
            }
            type = other.type;
            curr_row = other.curr_row;
            curr_col = other.curr_col;
            Matrix<T>::const_iterator& th = *this;
            return th;
        }

        template<class T>
        T& Matrix<T>::const_iterator::operator*() {
            T& ret = mat[curr_row][curr_col];
            return ret;
        }

        template<class T>
        const T& Matrix<T>::const_iterator::operator*() const {
            const T& ret = mat[curr_row][curr_col];
            return ret;
        }

        template<class T>
        typename Matrix<T>::const_iterator& Matrix<T>::const_iterator::operator++() {
            if (type == "row") {
                if (curr_row < h - 1) {
                    curr_row++;
                }
            } else {
                if (curr_col < w - 1) {
                    curr_col++;
                }
            }
            Matrix<T>::const_iterator& th = *this;
            return th;
        }

        template<class T>
        bool Matrix<T>::const_iterator::operator==(const const_iterator& rhs) {
            return this == &rhs;
        }

        template<class T>
        bool Matrix<T>::const_iterator::operator!=(const const_iterator& rhs) {
            return !(operator==(rhs));
        }

        template<class T>
        Matrix<T>::slice::slice() {

        }

        template<class T>
        Matrix<T>::slice::slice(string t, int pos, T** mat) {
            type = t;
            position = pos;
            cout << "slice(" << t << ", " << pos << ")" << endl;
            this->mat = mat;
        }

        template<class T>
        Matrix<T>::slice::slice(const slice& other) {
            type = other.type;
            position = other.position;
            mat = new T*[h];
            initMat(mat, h, w);
        }

        template<class T>
        T& Matrix<T>::slice::operator[](int i) {
            cout << "Position (operator[]): " << position << ", type == " << type << ", i == " << i << endl;
            if (type == "row") {
                // cout << h << " " << w << endl;
                if (i < 0 || i >= w) {
                    throw std::out_of_range("col must be 0 <= col < w");
                }
                T& ret = mat[position][i];

                exit(1);
                return ret;
            } else {
                if (i < 0 || i >= h) {
                    throw std::out_of_range("row must be 0 <= row < h");
                }
                T& ret = mat[i][position];
                return ret;
            }
        }

        template<class T>
        const T& Matrix<T>::slice::operator[](int i) const {
            if (type == "row") {
                if (i < 0 || i >= w) {
                    throw std::out_of_range("col must be 0 <= col < w");
                }
                const T& ret = mat[position][i];
                return ret;
            }
            else if (type == "col") {
                if (i < 0 || i >= h) {
                    throw std::out_of_range("row must be 0 <= row < h");
                }
                const T& ret = mat[i][position];
                return ret;
            }
        }

        template<class T>
        typename Matrix<T>::iterator Matrix<T>::slice::begin() {
            return Matrix<T>::iterator(type, position, 0);
        }

        template<class T>
        typename Matrix<T>::const_iterator Matrix<T>::slice::begin() const {
            return Matrix<T>::const_iterator(type, position);
        }

        template<class T>
        typename Matrix<T>::iterator Matrix<T>::slice::end() {
            if (type == "row") {
                return Matrix<T>::iterator(type, position, w - 1);
            } else {
                return Matrix<T>::iterator(type, position, h - 1);
            }
        }

        template<class T>
        typename Matrix<T>::const_iterator Matrix<T>::slice::end() const {
            if (type == "row") {
                return Matrix<T>::const_iterator(type, position, w - 1);
            } else {
                return Matrix<T>::const_iterator(type, position, h - 1);
            }
        }

        template<class T>
        Matrix<T>::const_slice::const_slice(string t, int pos) {
            type = t;
            position = pos;
        }

        template<class T>
        Matrix<T>::const_slice::const_slice(const const_slice& other) {
            type = other.type;
            position = other.position;
        }

        template<class T>
        T& Matrix<T>::const_slice::operator[](int i) {
            if (type == "row") {
                if (i < 0 || i >= w) {
                    throw std::out_of_range("col must be 0 <= col < w");
                }
                T& ret = mat[position][i];
                return ret;
            }
            else {
                if (i < 0 || i >= h) {
                    throw std::out_of_range("row must be 0 <= row < h");
                }
                T& ret = mat[i][position];
                return ret;
            }
        }

        template<class T>
        const T& Matrix<T>::const_slice::operator[](int i) const {
            if (type == "row") {
                if (i < 0 || i >= w) {
                    throw std::out_of_range("col must be 0 <= col < w");
                }
                const T& ret = mat[position][i];
                return ret;
            }
            else if (type == "col") {
                if (i < 0 || i >= h) {
                    throw std::out_of_range("row must be 0 <= row < h");
                }
                const T& ret = mat[i][position];
                return ret;
            }
        }

        template<class T>
        typename Matrix<T>::const_iterator Matrix<T>::const_slice::begin() const {
            return Matrix<T>::const_iterator(type, position);
        }

        template<class T>
        typename Matrix<T>::const_iterator Matrix<T>::const_slice::end() const {
            if (type == "row") {
                return Matrix<T>::const_iterator(type, position, w - 1);
            } else {
                return Matrix<T>::const_iterator(type, position, h - 1);
            }
        }


}

#endif

