// Bryan Owens
// CS427 HW #3: Matrix
// Due: 2/21/19

#ifndef __MATRIX_HPP
#define __MATRIX_HPP
#define FINAL (1)

#include <iostream>
#include <string>
using namespace std;

namespace debug
{
    bool flag = false;
    bool deepCopyMat = false;
}

namespace cs427527
{
    template<class T>
    class Matrix
    {
    public:

        class iterator {
        public:
            iterator(T* mat, string type, int pos, int h, int w, int curr);
            iterator(const iterator &other);
            iterator &operator=(const iterator &other);
            T& operator*();
            const T& operator*() const;
            iterator &operator++();
            bool operator==(const iterator &rhs);
            bool operator==(const iterator &rhs) const;
            bool operator!=(const iterator &rhs);
            bool operator!=(const iterator &rhs) const;

            T* mat;
            int h, w;
            int pos;
            string type;
            int curr_row, curr_col;
        };

        class const_iterator {
        public:
            const_iterator(T* mat, string type, int pos, int h, int w, int curr);
            const_iterator(const const_iterator &other);
            const_iterator &operator=(const const_iterator& other);
            T& operator*();
            const T& operator*() const;
            const_iterator& operator++();
            bool operator==(const const_iterator &rhs);
            bool operator==(const const_iterator &rhs) const;
            bool operator!=(const const_iterator &rhs);
            bool operator!=(const const_iterator &rhs) const;

            T* mat;
            int h, w;
            int pos;
            string type;
            int curr_row, curr_col;
        };

        class slice {
        public:
            slice();
            slice(T* mat, string type, int pos, int h, int w);
            slice(const slice& other);
            T& operator[](int i);
            const T& operator[](int i) const;
            iterator begin();
            const_iterator begin() const;
            iterator end();
            const_iterator end() const;

            T* mat;
            int h, w;
            string type;
            int pos;
        };

        class const_slice {
        public:
            const_slice();
            const_slice(T* mat, string type, int pos, int h, int w);
            const_slice(const const_slice& other);
            T& operator[](int i);
            const T& operator[](int i) const;
            const_iterator begin();
            const_iterator begin() const;
            const_iterator end();
            const_iterator end() const;

            T* mat;
            int h, w;
            string type;
            int pos;
        };

        void destroyMat(T* mat);
        Matrix();
        Matrix(int h, int w);
        Matrix(const Matrix& other);
        Matrix(Matrix&& other);
        Matrix& operator=(const Matrix& other);
        Matrix& operator=(Matrix&& other);
        int height();
        int height() const;
        int width();
        int width() const;
        T& at(int r, int c);
        const T& at(int r, int c) const;
        ~Matrix();
        slice operator[](int row);
        const const_slice operator[](int row) const;
        slice column(int col);
        const const_slice column(int col) const;

        T* mat;
        int h, w;
    };

    template<class T>
    void Matrix<T>::destroyMat(T* mat) {
        delete[] mat;
    }

    template<class T>
    Matrix<T>::Matrix() {
        int defaults = 1;
        this->h = defaults;
        this->w = defaults;
        this->mat = new T[this->h * this->w]();
    }

    template<class T>
    Matrix<T>::Matrix(int h, int w) {
        this->h = h;
        this->w = w;
        this->mat = new T[this->h * this->w]();
    }

    // Copy constructor
    template<class T>
    Matrix<T>::Matrix(const Matrix& other) {
        h = other.h;
        w = other.w;

        mat = new T[h * w]();
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                T val = other.mat[i * h + j];
                mat[i * h + j] = val;
            }
        }
    }

    // Move constructor
    template<class T>
    Matrix<T>::Matrix(Matrix&& other) {
        h = other.h;
        w = other.w;
        mat = other.mat;
        // make other's mat point somewhere else
        // so it doesn't corrupt the new object when it gets cleaned up
        other.mat = nullptr;
    }

    // Copy assignment operator
    // https://www.geeksforgeeks.org/copy-constructor-vs-assignment-operator-in-c/
    template<class T>
    Matrix<T>& Matrix<T>::operator=(const Matrix& other) {
        if (this == &other) {
            Matrix<T>& th = *this;
            return th;
        }
        h = other.h;
        w = other.w;
        destroyMat(mat);
        mat = new T[h * w]();
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                T val = other.at(i, j);
                mat[i * h + j] = val;
            }
        }
        Matrix<T>& th = *this;
        return th;
    }

    // Move assignment operator
    // https://docs.microsoft.com/en-us/cpp/cpp/move-constructors-and-move-assignment-operators-cpp?view=vs-2017
    template<class T>
    Matrix<T>& Matrix<T>::operator=(Matrix&& other) {
        if (this == &other) {
            Matrix<T>& th = *this;
            return th;
        }
        // destroyMat(mat, h, w);
        h = other.h;
        w = other.w;
        mat = other.mat;
        other.mat = nullptr;
        Matrix<T>& th = *this;
        return th;
    }

    template<class T>
    int Matrix<T>::height() {
        return h;
    }

    template<class T>
    int Matrix<T>::height() const {
        return h;
    }

    template<class T>
    int Matrix<T>::width() {
        return w;
    }

    template<class T>
    int Matrix<T>::width() const {
        return w;
    }

    template<class T>
    T& Matrix<T>::at(int r, int c) {
        if (r < 0 || r >= h) {
            throw std::out_of_range("r must be 0 <= r < h");
        }
        if (c < 0 || c >= w) {
            throw std::out_of_range("c must be 0 <= c < w");
        }
        T& ref = mat[r * w + c];
        return ref;
    }

    template<class T>
    const T& Matrix<T>::at(int r, int c) const {
        if (r < 0 || r >= h) {
            throw std::out_of_range("r must be 0 <= r < h");
        }
        if (c < 0 || c >= w) {
            throw std::out_of_range("c must be 0 <= c < w");
        }
        const T& ref = mat[r * w + c];
        return ref;
    }

    template<class T>
    Matrix<T>::~Matrix() {
        try {
            destroyMat(mat);
        } catch (...) {
        }
    }

    template<class T>
    typename Matrix<T>::slice Matrix<T>::operator[](int row) {
        if (row < 0 || row >= h) {
            throw std::out_of_range("row must be 0 <= row < h");
        }
        return slice(mat, "row", row, h, w);
    }

    template<class T>
    const typename Matrix<T>::const_slice Matrix<T>::operator[](int row) const {
        if (row < 0 || row >= h) {
            throw std::out_of_range("row must be 0 <= row < h");
        }
        return const_slice(mat, "row", row, h, w);
    }

    template<class T>
    typename Matrix<T>::slice Matrix<T>::column(int col) {
        if (col < 0 || col >= w) {
            throw std::out_of_range("col must be 0 <= col < w");
        }
        return slice(mat, "col", col, h, w);
    }

    template<class T>
    const typename Matrix<T>::const_slice Matrix<T>::column(int col) const {
        if (col < 0 || col >= w) {
            throw std::out_of_range("col must be 0 <= col < w");
        }
        return const_slice(mat, "col", col, h, w);
    }

        // Slice

        template<class T>
        Matrix<T>::slice::slice() {

        }

        template<class T>
        Matrix<T>::slice::slice(T* mat, string type, int pos, int h, int w) {
            this->mat = mat;
            this->type = type;
            this->pos = pos;
            this->h = h;
            this->w = w;
        }

        template<class T>
        Matrix<T>::slice::slice(const slice& other) {
            type = other.type;
            pos = other.pos;
            h = other.h;
            w = other.w;
            if (debug::deepCopyMat) {
                mat = new T[h * w]();
                for (int i = 0; i < h; i++) {
                    for (int j = 0; j < w; j++) {
                        T val = other.mat[i * h + j];
                        mat[i * h + j] = val;
                    }
                }
            } else {
                mat = other.mat;
            }

        }

        template<class T>
        T& Matrix<T>::slice::operator[](int i) {
            // cout << "Position (operator[]): " << pos << ", type == " << type << ", i == " << i << endl;
            if (type == "row") {
                // cout << h << " " << w << endl;
                if (i < 0 || i >= w) {
                    throw std::out_of_range("col must be 0 <= col < w");
                }
                T& ret = mat[pos * w + i];
                return ret;
            } else {
                if (i < 0 || i >= h) {
                    throw std::out_of_range("row must be 0 <= row < h");
                }
                T& ret = mat[i * w + pos];
                return ret;
            }
        }

        template<class T>
        const T& Matrix<T>::slice::operator[](int i) const {
            // cout << "Position (operator[]): " << pos << ", type == " << type << ", i == " << i << endl;
            if (type == "row") {
                if (i < 0 || i >= w) {
                    throw std::out_of_range("col must be 0 <= col < w");
                }
                const T& ret = mat[pos * w + i];
                return ret;
            } else {
                if (i < 0 || i >= h) {
                    throw std::out_of_range("row must be 0 <= row < h");
                }
                const T& ret = mat[i * w + pos];
                return ret;
            }
        }

        template<class T>
        typename Matrix<T>::iterator Matrix<T>::slice::begin() {
            return Matrix<T>::iterator(mat, type, pos, h, w, 0);
        }

        template<class T>
        typename Matrix<T>::const_iterator Matrix<T>::slice::begin() const {
            return Matrix<T>::const_iterator(mat, type, pos, h, w, 0);
        }

        template<class T>
        typename Matrix<T>::iterator Matrix<T>::slice::end() {
            if (type == "row") {
                return Matrix<T>::iterator(mat, type, pos, h, w, w);
            } else {
                return Matrix<T>::iterator(mat, type, pos, h, w, h);
            }
        }

        template<class T>
        typename Matrix<T>::const_iterator Matrix<T>::slice::end() const {
            if (type == "row") {
                return Matrix<T>::const_iterator(mat, type, pos, h, w, w);
            } else {
                return Matrix<T>::const_iterator(mat, type, pos, h, w, h);
            }
        }

            template<class T>
            Matrix<T>::iterator::iterator(T* mat, string type, int pos, int h, int w, int curr) {
                this->mat = mat;
                this->type = type;
                this->pos = pos;
                this->h = h;
                this->w = w;

                if (type == "row") {
                    curr_row = pos;
                    curr_col = curr;
                } else {
                    curr_row = curr;
                    curr_col = pos;
                }
            }

            template<class T>
            Matrix<T>::iterator::iterator(const iterator& other) {
                type = other.type;
                pos = other.pos;
                h = other.h;
                w = other.w;
                curr_row = other.curr_row;
                curr_col = other.curr_col;
                if (debug::deepCopyMat) {
                    mat = new T[h * w]();
                    for (int i = 0; i < h; i++) {
                        for (int j = 0; j < w; j++) {
                            T val = other.mat[i * h + j];
                            mat[i * h + j] = val;
                        }
                    }
                } else {
                    mat = other.mat;
                }

            }

            template<class T>
            typename Matrix<T>::iterator& Matrix<T>::iterator::operator=(const iterator& other) {
                if (this == &other) {
                    Matrix<T>::iterator& th = *this;
                    return th;
                }
                type = other.type;
                pos = other.pos;
                h = other.h;
                w = other.w;
                curr_row = other.curr_row;
                curr_col = other.curr_col;
                if (debug::deepCopyMat) {
                    mat = new T[h * w]();
                    for (int i = 0; i < h; i++) {
                        for (int j = 0; j < w; j++) {
                            T val = other.mat[i * h + j];
                            mat[i * h + j] = val;
                        }
                    }
                } else {
                    mat = other.mat;
                }

                Matrix<T>::iterator& th = *this;
                return th;
            }

            template<class T>
            T& Matrix<T>::iterator::operator*() {
                T& ret = mat[curr_row * w + curr_col];
                return ret;
            }

            template<class T>
            const T& Matrix<T>::iterator::operator*() const {
                const T& ret = mat[curr_row * w + curr_col];
                return ret;
            }

            template<class T>
            typename Matrix<T>::iterator& Matrix<T>::iterator::operator++() {
                if (type == "row") {
                    if (curr_col < w) {
                        curr_col++;
                    }
                } else {
                    if (curr_row < h) {
                        curr_row++;
                    }
                }
                Matrix<T>::iterator& th = *this;
                return th;
            }

            template<class T>
            bool Matrix<T>::iterator::operator==(const iterator& rhs) {
                // Check if positioned at the same element in the same matrix
                if (mat == rhs.mat && curr_row == rhs.curr_row && curr_col == rhs.curr_col) {
                    return true;
                }
                // Check if positioned past the last element in the same slice
                if (mat == rhs.mat && pos == rhs.pos && type == rhs.type) {
                    if (type == "row") {
                        if (curr_col >= w && rhs.curr_col >= w) {
                            return true;
                        }
                        return false;
                    }
                    if (type == "col") {
                        if (curr_row >= h && rhs.curr_row >= h) {
                            return true;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }

            template<class T>
            bool Matrix<T>::iterator::operator!=(const iterator& rhs) {
                return !(operator==(rhs));
            }

            template<class T>
            bool Matrix<T>::iterator::operator!=(const iterator& rhs) const {
                return !(operator==(rhs));
            }


            // Consts

                template<class T>
                Matrix<T>::const_slice::const_slice() {

                }

                template<class T>
                Matrix<T>::const_slice::const_slice(T* mat, string type, int pos, int h, int w) {
                    this->mat = mat;
                    this->type = type;
                    this->pos = pos;
                    this->h = h;
                    this->w = w;
                    // cout << "slice(" << type << ", " << pos << ")" << endl;
                }

                template<class T>
                Matrix<T>::const_slice::const_slice(const const_slice& other) {
                    type = other.type;
                    pos = other.pos;
                    h = other.h;
                    w = other.w;
                    if (debug::deepCopyMat) {
                        mat = new T[h * w]();
                        for (int i = 0; i < h; i++) {
                            for (int j = 0; j < w; j++) {
                                T val = other.mat[i * h + j];
                                mat[i * h + j] = val;
                            }
                        }
                    } else {
                        mat = other.mat;
                    }
                }

                template<class T>
                T& Matrix<T>::const_slice::operator[](int i) {
                    cout << "Position (operator[]): " << pos << ", type == " << type << ", i == " << i << endl;
                    if (type == "row") {
                        // cout << h << " " << w << endl;
                        if (i < 0 || i >= w) {
                            throw std::out_of_range("col must be 0 <= col < w");
                        }
                        T& ret = mat[pos * w + i];
                        return ret;
                    } else {
                        if (i < 0 || i >= h) {
                            throw std::out_of_range("row must be 0 <= row < h");
                        }
                        T& ret = mat[i * w + pos];
                        return ret;
                    }
                }

                template<class T>
                const T& Matrix<T>::const_slice::operator[](int i) const {
                    if (type == "row") {
                        if (i < 0 || i >= w) {
                            throw std::out_of_range("col must be 0 <= col < w");
                        }
                        T& ret = mat[pos * w + i];
                        return ret;
                    } else {
                        if (i < 0 || i >= h) {
                            throw std::out_of_range("row must be 0 <= row < h");
                        }
                        T& ret = mat[i * w + pos];
                        return ret;
                    }
                }

                template<class T>
                typename Matrix<T>::const_iterator Matrix<T>::const_slice::begin() {
                    return Matrix<T>::const_iterator(mat, type, pos, h, w, 0);
                }

                template<class T>
                typename Matrix<T>::const_iterator Matrix<T>::const_slice::begin() const {
                    return Matrix<T>::const_iterator(mat, type, pos, h, w, 0);
                }

                template<class T>
                typename Matrix<T>::const_iterator Matrix<T>::const_slice::end() {
                    if (type == "row") {
                        return Matrix<T>::const_iterator(mat, type, pos, h, w, w);
                    } else {
                        return Matrix<T>::const_iterator(mat, type, pos, h, w, h);
                    }
                }

                template<class T>
                typename Matrix<T>::const_iterator Matrix<T>::const_slice::end() const {
                    if (type == "row") {
                        return Matrix<T>::const_iterator(mat, type, pos, h, w, w);
                    } else {
                        return Matrix<T>::const_iterator(mat, type, pos, h, w, h);
                    }
                }

                // const_iterator

                template<class T>
                Matrix<T>::const_iterator::const_iterator(T* mat, string type, int pos, int h, int w, int curr) {
                    this->mat = mat;
                    this->type = type;
                    this->pos = pos;
                    this->h = h;
                    this->w = w;

                    if (type == "row") {
                        curr_row = pos;
                        curr_col = curr;
                    } else {
                        curr_row = curr;
                        curr_col = pos;
                    }
                }

                template<class T>
                Matrix<T>::const_iterator::const_iterator(const const_iterator& other) {
                    type = other.type;
                    pos = other.pos;
                    h = other.h;
                    w = other.w;
                    curr_row = other.curr_row;
                    curr_col = other.curr_col;
                    if (debug::deepCopyMat) {
                        mat = new T[h * w]();
                        for (int i = 0; i < h; i++) {
                            for (int j = 0; j < w; j++) {
                                T val = other.mat[i * h + j];
                                mat[i * h + j] = val;
                            }
                        }
                    } else {
                        mat = other.mat;
                    }

                }

                template<class T>
                typename Matrix<T>::const_iterator& Matrix<T>::const_iterator::operator=(const const_iterator& other) {
                    if (this == &other) {
                        Matrix<T>::const_iterator& th = *this;
                        return th;
                    }
                    type = other.type;
                    pos = other.pos;
                    h = other.h;
                    w = other.w;
                    curr_row = other.curr_row;
                    curr_col = other.curr_col;
                    if (debug::deepCopyMat) {
                        mat = new T[h * w]();
                        for (int i = 0; i < h; i++) {
                            for (int j = 0; j < w; j++) {
                                T val = other.mat[i * h + j];
                                mat[i * h + j] = val;
                            }
                        }
                    } else {
                        mat = other.mat;
                    }
                    Matrix<T>::const_iterator& th = *this;
                    return th;
                }


                template<class T>
                typename Matrix<T>::const_iterator& Matrix<T>::const_iterator::operator++() {
                    if (type == "row") {
                        if (curr_col <= w) {
                            curr_col++;
                        }
                    } else {
                        if (curr_row <= h) {
                            curr_row++;
                        }
                    }
                    Matrix<T>::const_iterator& th = *this;
                    return th;
                }

                template<class T>
                bool Matrix<T>::const_iterator::operator==(const const_iterator& rhs) {
                    if (debug::flag) {
                        cout << "(" << curr_row << ", " << curr_col << ") vs (" << rhs.curr_row << ", " << rhs.curr_col << ") ";
                    }
                    // Check if positioned at the same element in the same matrix
                    if (mat == rhs.mat && curr_row == rhs.curr_row && curr_col == rhs.curr_col) {
                        return true;
                    }
                    // Check if positioned past the last element in the same slice
                    if (mat == rhs.mat && pos == rhs.pos && type == rhs.type) {
                        if (type == "row") {
                            if (curr_col >= w && rhs.curr_col >= w) {
                                return true;
                            }
                            return false;
                        }
                        if (type == "col") {
                            if (curr_row >= h && rhs.curr_row >= h) {
                                return true;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }

                template<class T>
                bool Matrix<T>::const_iterator::operator==(const const_iterator& rhs) const {
                    // Check if positioned at the same element in the same matrix
                    if (mat == rhs.mat && curr_row == rhs.curr_row && curr_col == rhs.curr_col) {
                        return true;
                    }
                    // Check if positioned past the last element in the same slice
                    if (mat == rhs.mat && pos == rhs.pos && type == rhs.type) {
                        if (type == "row") {
                            if (curr_col >= w && rhs.curr_col >= w) {
                                return true;
                            }
                            return false;
                        }
                        if (type == "col") {
                            if (curr_row >= h && rhs.curr_row >= h) {
                                return true;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }

                template<class T>
                bool Matrix<T>::const_iterator::operator!=(const const_iterator& rhs) {
                    return !(operator==(rhs));
                }

                template<class T>
                bool Matrix<T>::const_iterator::operator!=(const const_iterator& rhs) const {
                    return !(operator==(rhs));
                }

                template<class T>
                T& Matrix<T>::const_iterator::operator*() {
                    T& ret = mat[curr_row * w + curr_col];
                    if (debug::flag) {
                        cout << "mat[" << curr_row << "][" << curr_col << "] ";
                    }
                    return ret;
                }

                template<class T>
                const T& Matrix<T>::const_iterator::operator*() const {
                    const T& ret = mat[curr_row * w + curr_col];
                    return ret;
                }


}

#endif

