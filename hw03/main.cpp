#include "matrix.hpp"
using namespace cs427527;

int main()
{

    typedef long T;
    Matrix<T> mat1(100, 5);
    cout << "mat1 has height == " << mat1.height() << endl;
    Matrix<T> mat2(90, 4);
    cout << "mat2 has height == " << mat2.height() << endl;

    cout << "Copy constructor test: ";
    // http://www.cplusplus.com/forum/beginner/190004/
    Matrix<T> mat3(mat2);
    mat2.at(0, 0) += 1;
    if (mat3.height() == mat2.height() && mat3.width() == mat2.width() && mat3.at(0, 0) != mat2.at(0, 0)) {
        cout << "Success" << endl;
    } else {
        cout << "FAIL" << endl;
    }

    cout << "Move constructor test: ";
    Matrix<T> mat4 = Matrix<T>(mat2);
    T old_mat2 = mat2.at(0, 0);
    T old_mat4 = mat4.at(0, 0);
    mat2.at(0, 0) += 1;
    cout << mat4.at(0, 0) << endl;
    if (mat4.height() == mat2.height() && mat2.width() == mat2.width() && old_mat2) {
        cout << "Success" << endl;
    } else {
        cout << "FAIL" << endl;
    }

    cout << "Copy assignment test: ";
    Matrix<T> mat5(20, 20);
    mat5 = mat2;
    cout << mat5.at(0, 0) << endl;

    cout << "Move assignment test: \n";

    for (int i = 0; i < mat2.height(); i++) {
        for (int j = 0; j < mat2.width(); j++) {
            cout << mat2.at(i, j) << " ";
        }
        cout << "\n";
    }
    cout << "————————" << endl;
    cout << "adsf: " << mat2.at(0, 0) << endl;
    mat2.at(1, 0) = 3;
    mat2.at(2, 0) = 4;
    auto m = mat2.column(0);
    cout << "height = " << mat2.height() << ", width = " << mat2.width() << endl;
//    cout << "m[0] == " << endl;
    cout << m.type << " " << m.pos << endl;
    cout << *(m.begin()) << endl;
    exit(1);
//    cout << *(m.begin()) << endl;
//    cout << *(m.begin()) << endl;
//    cout << "adsf: " << mat2.at(0, 0) << endl;
//    for (auto i = m.begin(); i != m.end(); i++) {
//        cout << *i << endl;
//    }
}

// Remove all the inheritance. You have to pass in the matrix as a parameter into the nested classes
// Change the matrix to an array to satisfy that O(1) destructor requirement
// Correct all the different const variations