[blo6@monkey cs427]$ /c/cs427/bin/submit 3 Makefile time.log matrix.hpp
Copying Makefile
/bin/cp: overwrite '/home/classes/cs427/SUBMIT/3/owens.bryan.blo6/Makefile'? y
Copying time.log
/bin/cp: overwrite '/home/classes/cs427/SUBMIT/3/owens.bryan.blo6/time.log'? y
Copying matrix.hpp
/bin/cp: overwrite '/home/classes/cs427/SUBMIT/3/owens.bryan.blo6/matrix.hpp'? y
[blo6@monkey cs427]$ ls
hw01  hw02  Makefile  matrix.hpp  matrix_unit.cpp  time.log  Unit
[blo6@monkey cs427]$ make
g++ -Wall -pedantic -std=c++17 -c matrix_unit.cpp
g++ -Wall -pedantic -std=c++17 -o Unit matrix_unit.o
[blo6@monkey cs427]$ make clean
rm *.o *~
rm: cannot remove '*~': No such file or directory
make: *** [Makefile:19: clean] Error 1
[blo6@monkey cs427]$ /c/cs427/bin/submit 3 Makefile time.log matrix.hpp
Copying Makefile
/bin/cp: overwrite '/home/classes/cs427/SUBMIT/3/owens.bryan.blo6/Makefile'? y
Copying time.log
/bin/cp: overwrite '/home/classes/cs427/SUBMIT/3/owens.bryan.blo6/time.log'? y
Copying matrix.hpp
/bin/cp: overwrite '/home/classes/cs427/SUBMIT/3/owens.bryan.blo6/matrix.hpp'? y
[blo6@monkey cs427]$ for N in `seq 0 15`; do valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes ./Unit $N; done
==29530== Memcheck, a memory error detector
==29530== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29530== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29530== Command: ./Unit 0
==29530== 
2x2 IDENTITY MATRIX
1 0 
0 1 
==29530== 
==29530== HEAP SUMMARY:
==29530==     in use at exit: 0 bytes in 0 blocks
==29530==   total heap usage: 3 allocs, 3 frees, 73,744 bytes allocated
==29530== 
==29530== All heap blocks were freed -- no leaks are possible
==29530== 
==29530== For counts of detected and suppressed errors, rerun with: -v
==29530== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29531== Memcheck, a memory error detector
==29531== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29531== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29531== Command: ./Unit 1
==29531== 
2x2 IDENTITY MATRIX
1 0 
0 1 
==29531== 
==29531== HEAP SUMMARY:
==29531==     in use at exit: 0 bytes in 0 blocks
==29531==   total heap usage: 3 allocs, 3 frees, 73,744 bytes allocated
==29531== 
==29531== All heap blocks were freed -- no leaks are possible
==29531== 
==29531== For counts of detected and suppressed errors, rerun with: -v
==29531== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29532== Memcheck, a memory error detector
==29532== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29532== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29532== Command: ./Unit 2
==29532== 
ORIGINAL
99 0 0 0 
0 98 0 0 
0 0 0 0 
COPY
99 0 0 0 
0 98 0 0 
0 0 0 0 
==29532== 
==29532== HEAP SUMMARY:
==29532==     in use at exit: 0 bytes in 0 blocks
==29532==   total heap usage: 4 allocs, 4 frees, 73,824 bytes allocated
==29532== 
==29532== All heap blocks were freed -- no leaks are possible
==29532== 
==29532== For counts of detected and suppressed errors, rerun with: -v
==29532== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29533== Memcheck, a memory error detector
==29533== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29533== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29533== Command: ./Unit 3
==29533== 
ORIGINAL
0 0 0 0 
0 0 0 0 
0 0 0 0 
MODIFIED COPY
99 0 0 0 
0 98 0 0 
0 0 0 0 
==29533== 
==29533== HEAP SUMMARY:
==29533==     in use at exit: 0 bytes in 0 blocks
==29533==   total heap usage: 4 allocs, 4 frees, 73,824 bytes allocated
==29533== 
==29533== All heap blocks were freed -- no leaks are possible
==29533== 
==29533== For counts of detected and suppressed errors, rerun with: -v
==29533== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29534== Memcheck, a memory error detector
==29534== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29534== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29534== Command: ./Unit 4
==29534== 
RESET COPY TO ORIGINAL
0 0 0 0 
0 0 0 0 
0 0 0 0 
==29534== 
==29534== HEAP SUMMARY:
==29534==     in use at exit: 0 bytes in 0 blocks
==29534==   total heap usage: 5 allocs, 5 frees, 73,872 bytes allocated
==29534== 
==29534== All heap blocks were freed -- no leaks are possible
==29534== 
==29534== For counts of detected and suppressed errors, rerun with: -v
==29534== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29535== Memcheck, a memory error detector
==29535== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29535== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29535== Command: ./Unit 5
==29535== 
3x3 MULTIPLICATION TABLE
0 0 0 
0 1 2 
0 2 4 
==29535== 
==29535== HEAP SUMMARY:
==29535==     in use at exit: 0 bytes in 0 blocks
==29535==   total heap usage: 3 allocs, 3 frees, 73,764 bytes allocated
==29535== 
==29535== All heap blocks were freed -- no leaks are possible
==29535== 
==29535== For counts of detected and suppressed errors, rerun with: -v
==29535== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29536== Memcheck, a memory error detector
==29536== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29536== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29536== Command: ./Unit 6
==29536== 
middle row, last column: 33
==29536== 
==29536== HEAP SUMMARY:
==29536==     in use at exit: 0 bytes in 0 blocks
==29536==   total heap usage: 3 allocs, 3 frees, 73,776 bytes allocated
==29536== 
==29536== All heap blocks were freed -- no leaks are possible
==29536== 
==29536== For counts of detected and suppressed errors, rerun with: -v
==29536== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29538== Memcheck, a memory error detector
==29538== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29538== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29538== Command: ./Unit 7
==29538== 
PRINT USING SLICES
99 0 0 0 
0 98 2 3 
0 2 4 6 
==29538== 
==29538== HEAP SUMMARY:
==29538==     in use at exit: 0 bytes in 0 blocks
==29538==   total heap usage: 3 allocs, 3 frees, 73,776 bytes allocated
==29538== 
==29538== All heap blocks were freed -- no leaks are possible
==29538== 
==29538== For counts of detected and suppressed errors, rerun with: -v
==29538== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29539== Memcheck, a memory error detector
==29539== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29539== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29539== Command: ./Unit 8
==29539== 
PRINT TRANSPOSED USING COLUMN SLICES
99 0 0 
0 98 2 
0 2 4 
0 3 6 
==29539== 
==29539== HEAP SUMMARY:
==29539==     in use at exit: 0 bytes in 0 blocks
==29539==   total heap usage: 3 allocs, 3 frees, 73,776 bytes allocated
==29539== 
==29539== All heap blocks were freed -- no leaks are possible
==29539== 
==29539== For counts of detected and suppressed errors, rerun with: -v
==29539== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29540== Memcheck, a memory error detector
==29540== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29540== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29540== Command: ./Unit 9
==29540== 
ORIGINAL WITH ITERATORS
99 0 0 0 
0 98 2 3 
0 2 4 6 
==29540== 
==29540== HEAP SUMMARY:
==29540==     in use at exit: 0 bytes in 0 blocks
==29540==   total heap usage: 3 allocs, 3 frees, 73,776 bytes allocated
==29540== 
==29540== All heap blocks were freed -- no leaks are possible
==29540== 
==29540== For counts of detected and suppressed errors, rerun with: -v
==29540== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29541== Memcheck, a memory error detector
==29541== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29541== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29541== Command: ./Unit 10
==29541== 
COPY AFTER MODIFYING LAST COLUMN
99 0 0 42 
0 98 2 42 
0 2 4 42 
==29541== 
==29541== HEAP SUMMARY:
==29541==     in use at exit: 0 bytes in 0 blocks
==29541==   total heap usage: 3 allocs, 3 frees, 73,776 bytes allocated
==29541== 
==29541== All heap blocks were freed -- no leaks are possible
==29541== 
==29541== For counts of detected and suppressed errors, rerun with: -v
==29541== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29542== Memcheck, a memory error detector
==29542== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29542== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29542== Command: ./Unit 11
==29542== 
SUM OF ORIGINAL BOTTOM ROW
12
==29542== 
==29542== HEAP SUMMARY:
==29542==     in use at exit: 0 bytes in 0 blocks
==29542==   total heap usage: 3 allocs, 3 frees, 73,776 bytes allocated
==29542== 
==29542== All heap blocks were freed -- no leaks are possible
==29542== 
==29542== For counts of detected and suppressed errors, rerun with: -v
==29542== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29543== Memcheck, a memory error detector
==29543== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29543== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29543== Command: ./Unit 12
==29543== 
SUM OF BOTTOM ROW OF CONST REFERENCE
12
==29543== 
==29543== HEAP SUMMARY:
==29543==     in use at exit: 0 bytes in 0 blocks
==29543==   total heap usage: 3 allocs, 3 frees, 73,776 bytes allocated
==29543== 
==29543== All heap blocks were freed -- no leaks are possible
==29543== 
==29543== For counts of detected and suppressed errors, rerun with: -v
==29543== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29544== Memcheck, a memory error detector
==29544== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29544== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29544== Command: ./Unit 13
==29544== 
SUM OF ENTIRE ORIGINAL
18
==29544== 
==29544== HEAP SUMMARY:
==29544==     in use at exit: 0 bytes in 0 blocks
==29544==   total heap usage: 3 allocs, 3 frees, 73,776 bytes allocated
==29544== 
==29544== All heap blocks were freed -- no leaks are possible
==29544== 
==29544== For counts of detected and suppressed errors, rerun with: -v
==29544== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29545== Memcheck, a memory error detector
==29545== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29545== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29545== Command: ./Unit 14
==29545== 
ITERATOR COMPARISONS
begin == begin: 1
iterator == const iterator: 1
same position, different matrices: 0
same position through row and column: 1
==29545== 
==29545== HEAP SUMMARY:
==29545==     in use at exit: 0 bytes in 0 blocks
==29545==   total heap usage: 6 allocs, 6 frees, 73,920 bytes allocated
==29545== 
==29545== All heap blocks were freed -- no leaks are possible
==29545== 
==29545== For counts of detected and suppressed errors, rerun with: -v
==29545== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==29580== Memcheck, a memory error detector
==29580== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==29580== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==29580== Command: ./Unit 15
==29580== 
FINAL is defined
==29580== 
==29580== HEAP SUMMARY:
==29580==     in use at exit: 0 bytes in 0 blocks
==29580==   total heap usage: 2 allocs, 2 frees, 73,728 bytes allocated
==29580== 
==29580== All heap blocks were freed -- no leaks are possible
==29580== 
==29580== For counts of detected and suppressed errors, rerun with: -v
==29580== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
[blo6@monkey cs427]$ /c/cs427/bin/protect 3 Makefile time.log matrix.hpp
Protecting Makefile
Protecting time.log
Protecting matrix.hpp
[blo6@monkey cs427]$ /c/cs427/bin/check 3
total 40
-r--r----- 1 blo6 cs427ta   329 Feb 22 05:37 Makefile
-r--r----- 1 blo6 cs427ta 26283 Feb 22 05:37 matrix.hpp
-r--r----- 1 blo6 cs427ta  2192 Feb 22 05:37 time.log
[blo6@monkey cs427]$ /c/cs427/bin/makeit 3
Copying matrix_unit.cpp
Making 
g++ -Wall -pedantic -std=c++17 -c matrix_unit.cpp
g++ -Wall -pedantic -std=c++17 -o Unit matrix_unit.o
[blo6@monkey cs427]$ /c/cs427/bin/testit 3 Unit
/home/classes/cs427/Hwk3/test.Unit
Copying matrix_unit.cpp
Executing /home/classes/cs427/Hwk3/test.Unit

Public test script for Unit (02/10/2019)

***** Checking for warning messages *****
Making -B ./Unit
g++ -Wall -pedantic -std=c++17 -c matrix_unit.cpp
g++ -Wall -pedantic -std=c++17 -o Unit matrix_unit.o

Each test is either passed or failed; there is no partial credit.

To execute the test labelled IJ, type the command:
     /c/cs427/hw3/Tests/tIJ
 or      ./Unit < /c/cs427/hw3/Tests/tIJ
The answer expected is in /c/cs427/hw3/Tests/tIJ.out.


           Tests using Matrix class only (define MATRIX symbol)
  1 point  001. Set elements with at
  1 point  002. Print matrix passed as const reference

           Tests using Matrix class only (define MATRIX symbol): 2 points

           Tests using copy/move constructors/assignment (define MATRIXCOPY symbol)
  1 point  003. Copy constructor
  1 point  004. Copy is not alias of original
  1 point  005. Copy assignment
  1 point  006. Copy constructor with valgrind
  1 point  007. Copy assignment with valgrind

           Tests using copy/move constructors/assignment (define MATRIXCOPY symbol): 5 points

           Tests using slices (define SLICE symbol)
  1 point  008. Set elements through slice view
  1 point  009. Access element through const_slice
  1 point  010. Print using row slices
  1 point  011. Print using column slices
  1 point  012. Set elements with valgrind
  1 point  013. Print using column slices with valgrind

           Tests using slices (define SLICE symbol): 6 points

           Tests with iterators (define ALL symbol)
  1 point  014. Print with iterators
  1 point  015. Set column with iterators
  1 point  016. Sum slice with iterators
  1 point  017. Sum const_slice with iterators
  1 point  018. Sum with const_slices
  1 point  019. Iterator comparisons
  1 point  020. Set column with iterators
  1 point  021. Sum slice with iterators
  1 point  022. Sum const_slice with iterators
  1 point  023. Sum with const_slices

           Tests with iterators (define ALL symbol): 10 points

           Final tests (define FINAL symbol)
  1 point  024. Final test

           Final tests (define FINAL symbol): 1 points

               Deductions for Violating Specification (0 => no violation)

End of Public Script

 24 points Total for Unit

           Possible Deductions (assessed later as appropriate)
                -5 Does not make
                -5 Makefile missing
                -5 Makefile incorrect
                -1 Log file incorrectly named
                -1 Log file lacks estimated time
                -1 Log file lacks total time
                -1 Log file lacks statement of major difficulties
                -1 Compilation errors using -Wall -std=c++17 -pedantic

***** Checking log file *****
Estimate: ESTIMATE of time to complete assignment: 7 hrs.
Total: TOTAL: 17 hrs.

***** Checking makefile *****
[blo6@monkey cs427]$ /c/cs427/bin/check 3
total 40
-r--r----- 1 blo6 cs427ta   329 Feb 22 05:37 Makefile
-r--r----- 1 blo6 cs427ta 26283 Feb 22 05:37 matrix.hpp
-r--r----- 1 blo6 cs427ta  2192 Feb 22 05:37 time.log
