#include "spinout.hpp"

#define ERROR_FLAG (0)
#define SUCCESS_FLAG (0)
#include <cstring>
using namespace cs427_527;

int main(int argc, char** argv)
{
    try {
        bool interactive_mode = false;
        SpinOut game;

        // Process command-line args
        if (argc == 1) {
            game = SpinOut();
            if (game.isSolved()) {
                cout << "SOLVED" << endl;
                return SUCCESS_FLAG;
            }
            cout << game.toString() << endl;
            return SUCCESS_FLAG;
        }
        if (strncmp(argv[1], "-i", 2) == 0) {
            interactive_mode = true;
        }

        if (interactive_mode) {
            if (argc <= 2) {
                game = SpinOut();
            } else {
                game = SpinOut(argv[2]);
            }
            cout << game.toString() << endl;
            if (game.isSolved()) {
                cout << game.totalMoves() << " moves" << endl;
                return SUCCESS_FLAG;
            }
            int move;
            while (cin >> move) {
                if (!(game.isLegalMove(move))) {
                    cout << "illegal move" << endl;
                    continue;
                }
                game.makeMove(move);
                cout << game.toString() << endl;
                if (game.isSolved()) {
                    cout << game.totalMoves() << " moves" << endl;
                    return SUCCESS_FLAG;
                }
            }
        } else {
            // Not interactive mode
            int first_move_index = 1;
            if (game.isValidStartConfig(argv[1])) {
                game = SpinOut(argv[1]);
                first_move_index = 2;
            } else {
                game = SpinOut();
            }
            for (int i = first_move_index; i < argc; i++) {
                int move = atoi(argv[i]);
                if (!(game.isLegalMove(move))) {
                    cout << "SpinOut: illegal move " << move << " in position " << i - first_move_index + 1 << " for " << game.toString() << endl;
                    return SUCCESS_FLAG;
                }
                game.makeMove(move);
            }
            if (game.isSolved()) {
                cout << "SOLVED" << endl;
                return SUCCESS_FLAG;
            }
            cout << game.toString() << endl;
            return SUCCESS_FLAG;
        }
    } catch (...) {
        // cout << "SpinOut: An exception occurred" << endl;
    }
}