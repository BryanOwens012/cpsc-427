#ifndef __SPINOUT_HPP
#define __SPINOUT_HPP

#include <iostream>
#include <string>
using namespace std;

namespace cs427_527
{
    class SpinOut
    {
    public:
        /* *
         * Initializes the puzzle so all disks are vertical.
         */
        SpinOut();

        /* *
         * Initializes the puzzle according to the given string.
         * The string will consist of 7 characters, each of which
         * is '/' or '-' respectively to indicate a disk
         * that is intially vertical or horizontal respectively.
         *
         * @param s: a string
         */
        SpinOut(const string& s);

        /* *
         * Takes an integer between 0 and 6 inclusive and determines
         * whether it is possible to rotate that disk
         * in the current object to its other orientation.
         * Disks are numbered from left to right starting with zero.
         *
         * @param i: an integer
         *
         * @return: true or false, whether it is possible
         */
        bool isLegalMove(int i) const;

        /* *
         * Takes an integer for which isLegalMove returns true
         * and rotates the indicated disk
         * in the current object into its other orientation.
         */
        void makeMove(int i);

        /* *
         * Counts the number of legal moves performed
         * on the current object since it was created.
         */
        int totalMoves() const;

        /* *
         * Determines if the current object is in
         * its solved configuration (all disks rotated horizontally).
         */
        bool isSolved() const;

        /* *
         * Returns a string representation of the current object in the same format
         * as that passed to the second version of the constructor.
         */
        string toString() const;

        /* *
         * Determines if a start config is valid
         *
         * @param s: a string
         */
        bool isValidStartConfig(const string& s) const;

        static const int SIZE = 7;
        int total_moves = 0;
        string disks;
    };
}

#endif