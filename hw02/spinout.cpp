#include "spinout.hpp"

namespace cs427_527
{

    SpinOut::SpinOut() {
        disks = "///////";
    }

    SpinOut::SpinOut(const string& s) {
        // The string will consist of 7 characters, each of which is '/' or '-'
        if (!(isValidStartConfig(s))) {
            throw std::invalid_argument("SpinOut: string must be of length 7 and contain only '/' and '-'");
        }
        disks = s;
    }

    bool SpinOut::isLegalMove(int i) const {
        if (!(i >= 0 && i <= 6)) {
            throw std::out_of_range("SpinOut: integer must be in range [0,6] inclusive");
        }
        // If it's the rightmost disk, it can always be rotated
        if (i == 6) {
            return true;
        }
        /* *
         * If it's the second to rightmost disk, it can be rotated
         * if the one to its right is vertical
        */
        if (i == 5) {
            if (disks[6] == '/') {
                return true;
            }
            return false;
        }
        // If 0 <= i <= 4
        if (disks[i+1] != '/') {
            return false;
        }
        for (int j = i+2; j < SIZE; j++) {
            if (disks[j] != '-') {
                return false;
            }
        }
        return true;
    }

    void SpinOut::makeMove(int i) {
        if (disks[i] == '/') {
            disks[i] = '-';
        } else {
            disks[i] = '/';
        }
        total_moves++;
    }

    int SpinOut::totalMoves() const {
        return total_moves;
    }

    bool SpinOut::isSolved() const {
        for (int i = 0; i < SIZE; i++) {
            if (disks[i] != '-') {
                return false;
            }
        }
        return true;
    }

    string SpinOut::toString() const {
        return disks;
    }

    bool SpinOut::isValidStartConfig(const string& s) const {
        if (s.length() != 7) {
            return false;
        }
        for (int i = 0; i < SIZE; i++) {
            if (!(s[i] == '/' || s[i] == '-')) {
                return false;
            }
        }
        return true;
    }
}