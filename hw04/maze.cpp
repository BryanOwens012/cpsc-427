#ifndef __MAZE_CPP
#define __MAZE_CPP

namespace cs427_527 {
    Maze::Maze() {
    }

    Maze::Maze(vector<string> input) {
        this->input = input;
        h = input.size();
        w = input[0].size();
    }

    string Maze::makeKey(int r, int c, string orientation) {
        string key = to_string(r) + "r" + to_string(c) + "c" + orientation;
        return key;
    }

    bool Maze::inSet(unordered_set<string>& uo_set, Maze::State& s) {
        string key = makeKey(s.r, s.c, s.orientation);
        if (uo_set.find(key) != uo_set.end()) {
            return true;
        }
        return false;
    }

    vector<string> Maze::shortestPath() {
        for (int i = 0; i < h; i++) {
            if (i == 0) {
                for (int j = 0; j < w; j++) {
                    if (j == 0) { // top left elt
                        q.push(Maze::State(i, j, "east")); // left
//                        predecessors[Maze::State(i, j, "east")] = Maze::State(i, j-1, "east");
                        predecessors[Maze::makeKey(i, j, "east")] = Maze::State(i, j-1, "east");
                    }
                    if (j == w-1) { // top right elt
                        q.push(Maze::State(i, j, "west")); // right
//                        predecessors[Maze::State(i, j, "west")] = Maze::State(i, j+1, "west");
                        predecessors[Maze::makeKey(i, j, "west")] = Maze::State(i, j+1, "west");
                    }
                    q.push(Maze::State(i, j, "south")); // top
                    predecessors[Maze::makeKey(i, j, "south")] = Maze::State(i-1, j, "south");
                }
            }
            if (i == h-1) {
                for (int j = 0; j < w; j++) {
                    if (j == 0) { // bottom left elt
                        q.push(Maze::State(i, j, "east")); // left
                        predecessors[Maze::makeKey(i, j, "east")] = Maze::State(i, j-1, "east");
                    }
                    if (j == w-1) { // top right elt
                        q.push(Maze::State(i, j, "west")); // right
                        predecessors[Maze::makeKey(i, j, "west")] = Maze::State(i, j+1, "west");
                    }
                    q.push(Maze::State(i, j, "north")); // bottom
                    predecessors[Maze::makeKey(i, j, "north")] = Maze::State(i+1, j, "north");
                }
            }
            if (!(i == 0 || i == h-1)) {
                q.push(Maze::State(i, 0, "east")); // left
                predecessors[Maze::makeKey(i, 0, "east")] = Maze::State(i, -1, "east");
                q.push(Maze::State(i, h-1, "west")); // right
                predecessors[Maze::makeKey(i, h-1, "west")] = Maze::State(i, h, "west");
            }
        }
        Maze::State solution = bfs();
        if (solution.orientation != "no_solution") {
            cout << "yay!" << endl;
        }
        vector<string> v;
        v.push_back("(1, 1)");
        return v;
    }

//    pair<int, int> Maze::getNext(State s, char prev_orient, char curr_orient) {
//        pair<int, int> next = make_pair(s.r, s.c);
//        if (prev_orient == '')
//    }

    Maze::State Maze::bfs() {
        while (q.empty()) {
            Maze::State s = q.front();
            q.pop();
            int r = s.r;
            int c = s.c;
            string orientation = s.orientation;
//            if (r < 0 || c < 0 || r >= h || r >= w) {
//                continue;
//            }
            if (inSet(visited, s) || input[r][c] == 'X') {
                continue;
            }
            if (input[r][c] == '*') {
                cout << "yay" << endl;
                return s;
            }
            else if (input[r][c] == '?') {
                q.push(Maze::State(r-1, c, "north"));
                predecessors[Maze::makeKey(r-1, c, "north")] = s;
                q.push(Maze::State(r+1, c, "south"));
                predecessors[Maze::makeKey(r+1, c, "south")] = s;
                q.push(Maze::State(r, c-1, "west"));
                predecessors[Maze::makeKey(r, c-1, "west")] = s;
                q.push(Maze::State(r, c+1, "east"));
                predecessors[Maze::makeKey(r, c+1, "east")] = s;
            }
            else if (input[r][c] == 'L') {
                if (orientation == "north") {
                    q.push(Maze::State(r, c-1, "west"));
                    predecessors[Maze::makeKey(r, c-1, "west")] = s;
                }
                else if (orientation == "south") {
                    q.push(Maze::State(r, c+1, "east"));
                    predecessors[Maze::makeKey(r, c+1, "east")] = s;
                }
                else if (orientation == "east") {
                    q.push(Maze::State(r-1, c, "north"));
                    predecessors[Maze::makeKey(r-1, c, "north")] = s;
                }
                else if (orientation == "west") {
                    q.push(Maze::State(r+1, c, "south"));
                    predecessors[Maze::makeKey(r+1, c, "south")] = s;
                }
            }
            else if (input[r][c] == 'R') {
                if (orientation == "north") {
                    q.push(Maze::State(r, c+1, "east"));
                    predecessors[Maze::makeKey(r, c+1, "east")] = s;
                }
                else if (orientation == "south") {
                    q.push(Maze::State(r, c-1, "west"));
                    predecessors[Maze::makeKey(r, c-1, "west")] = s;
                }
                else if (orientation == "east") {
                    q.push(Maze::State(r+1, c, "south"));
                    predecessors[Maze::makeKey(r+1, c, "south")] = s;
                }
                else if (orientation == "west") {
                    q.push(Maze::State(r-1, c, "north"));
                    predecessors[Maze::makeKey(r+1, c, "south")] = s;
                }
            }
            else if (input[r][c] == 'S') {
                if (orientation == "north") {
                    q.push(Maze::State(r-1, c, "north"));
                    predecessors[Maze::makeKey(r-1, c, "north")] = s;
                }
                else if (orientation == "south") {
                    q.push(Maze::State(r+1, c, "south"));
                    predecessors[Maze::makeKey(r+1, c, "south")] = s;
                }
                else if (orientation == "east") {
                    q.push(Maze::State(r, c+1, "east"));
                    predecessors[Maze::makeKey(r, c+1, "east")] = s;
                }
                else if (orientation == "west") {
                    q.push(Maze::State(r, c-1, "west"));
                    predecessors[Maze::makeKey(r, c-1, "west")] = s;
                }
            }
            else if (input[r][c] == 'U') {
                if (orientation == "north") {
                    q.push(Maze::State(r+1, c, "south"));
                    predecessors[Maze::makeKey(r+1, c, "south")] = s;
                }
                else if (orientation == "south") {
                    q.push(Maze::State(r-1, c, "north"));
                    predecessors[Maze::makeKey(r-1, c, "north")] = s;
                }
                else if (orientation == "east") {
                    q.push(Maze::State(r, c-1, "west"));
                    predecessors[Maze::makeKey(r, c-1, "west")] = s;
                }
                else if (orientation == "west") {
                    q.push(Maze::State(r, c+1, "east"));
                    predecessors[Maze::makeKey(r, c+1, "east")] = s;
                }
            }
//                q.push(Maze::State(r-1, c-1));
//                q.push(Maze::State(r-1, c));
//                q.push(Maze::State(r-1, c+1));
//                q.push(Maze::State(r, c-1));
//                q.push(Maze::State(r, c+1));
//                q.push(Maze::State(r+1, c-1));
//                q.push(Maze::State(r))
            visited.insert(Maze::makeKey(s.r, s.c, s.orientation));
        }
        return State(-1, -1, "no_solution");
    }

    bool Maze::State::operator==(const Maze::State& rhs) const {
        if (r == rhs.r && c == rhs.c && orientation == rhs.orientation) {
            return true;
        }
        return false;
    }

    Maze::State::State() {
    }
}

//namespace std {
//    template<>
//    class std::hash<cs427_527::Maze::State> {
//    public:
//        size_t operator()(const State& s) {
//            size_t str_hash = 0;
//            for (char c : s.orientation) {
//                str_hash += int(c);
//            }
//            return s.r * 37 + s.c * 17 + str_hash * 7;
//        }
//    };
//}

#endif