// Bryan Owens
// CS427 HW #4: Maze
// Due: 3/7/19

#ifndef __MAZE_HPP
#define __MAZE_HPP

#include <iostream>
#include <algorithm>
#include <utility>
#include <tuple>
#include <queue>
#include <string>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <climits>

using namespace std;

namespace cs427_527 {
    class Maze {
    public:
        Maze();
        Maze(vector<string> input);
        vector<string> shortestPath();

        class State {
        public:
            State();
            State(int r, int c, string orientation, string turn);
            bool operator==(const State& rhs) const;
            int r;
            int c;
            string orientation, turn;

            friend ostream& operator<<(ostream& os, const State& s);
        };

        bool inSet(unordered_set<string>& uo_set, State& s);
        bool inMap(unordered_map<string, State>& uo_map, Maze::State& s);
        string makeKey(int r, int c, string orientation, string turn);
        State bfs();
        void traceBack(State goal_state, vector<string>& solution);

        vector<string> input;
        int h, w;
        unordered_map<string, State> predecessors;
        unordered_set<string> visited;
        queue<State> q;

        void qpush(const State& s);
    };

//namespace std
//{
//    template<>
//    struct hash<cs427_527::Maze::State>
//    {
//        size_t operator()(const cs427_527::Maze::State& s) const {
////            using std::size_t;
////            using std::hash;
////            using std::string;
////            size_t h1 = hash<int>{}(s.r);
////            size_t h2 = hash<int>{}(s.c);
////            size_t h3 = hash<string>{}(s.orientation);
////            size_t k = 0xC6A4A7935BD1E995UL;
////            return (h1 ^ ((h2 * k) >> 47) * k) * k;
//            return 0;
//        }
//    };
//}

    Maze::Maze() {
    }

    Maze::Maze(vector<string> input) {
        this->input = input;
        h = input.size();
        w = input[0].size();
    }

    string Maze::makeKey(int r, int c, string orientation, string turn) {
        string key = to_string(r) + "r" + to_string(c) + "c" + orientation + "o" + turn;
        return key;
    }

    bool Maze::inSet(unordered_set<string>& uo_set, Maze::State& s) {
        string key = makeKey(s.r, s.c, s.orientation, s.turn);
        if (uo_set.find(key) != uo_set.end()) {
            return true;
        }
        return false;
    }

    bool Maze::inMap(unordered_map<string, Maze::State>& uo_map, Maze::State& s) {
        string key = makeKey(s.r, s.c, s.orientation, s.turn);
        if (uo_map.find(key) != uo_map.end()) {
            return true;
        }
        return false;
    }

    vector<string> Maze::shortestPath() {
        for (int i = 0; i < h; i++) {
            if (i == 0) {
                for (int j = 0; j < w; j++) {
                    if (j == 0) { // top left elt
                        qpush(Maze::State(i, j, "east", "S")); // left
//                        predecessors.insert({Maze::State(i, j, "east", "S")] = Maze::State(i, j-1, "east");
                        predecessors.insert({Maze::makeKey(i, j, "east", "S"), Maze::State(i, j-1, "east", "S")});
                    }
                    if (j == w-1) { // top right elt
                        qpush(Maze::State(i, j, "west", "S")); // right
//                        predecessors.insert({Maze::State(i, j, "west")] = Maze::State(i, j+1, "west");
                        predecessors.insert({Maze::makeKey(i, j, "west", "S"), Maze::State(i, j+1, "west", "S")});
                    }
                    qpush(Maze::State(i, j, "south", "S")); // top
                    predecessors.insert({Maze::makeKey(i, j, "south", "S"), Maze::State(i-1, j, "south", "S")});
                }
            }
            if (i == h-1) {
                for (int j = 0; j < w; j++) {
                    if (j == 0) { // bottom left elt
                        qpush(Maze::State(i, j, "east", "S")); // left
                        predecessors.insert({Maze::makeKey(i, j, "east", "S"), Maze::State(i, j-1, "east", "S")});
                    }
                    if (j == w-1) { // top right elt
                        qpush(Maze::State(i, j, "west", "S")); // right
                        predecessors.insert({Maze::makeKey(i, j, "west", "S"), Maze::State(i, j+1, "west", "S")});
                    }
                    qpush(Maze::State(i, j, "north", "S")); // bottom
                    predecessors.insert({Maze::makeKey(i, j, "north", "S"), Maze::State(i+1, j, "north", "S")});
                }
            }
            if (!(i == 0 || i == h-1)) {
                qpush(Maze::State(i, 0, "east", "S")); // left
//                cout << i << " " << 0 << " east" << endl;
                predecessors.insert({Maze::makeKey(i, 0, "east", "S"), Maze::State(i, -1, "east", "S")});
                qpush(Maze::State(i, w-1, "west", "S")); // right
                predecessors.insert({Maze::makeKey(i, w-1, "west", "S"), Maze::State(i, w, "west", "S")});
            }
        }
//        qpush(Maze::State(1, 0, "east", "S"));
//        predecessors.insert({Maze::makeKey(1, 0, "east", "S")] = Maze::State(1, -1, "east", "S");
        Maze::State goal_state = Maze::bfs();
        vector<string> solution;
        if (goal_state.orientation == "no_solution") {
            return solution;
        }
        // string coord_str = "(" + to_string(goal_state.r) + ", " + to_string(goal_state.c) + ", " + goal_state.orientation + ")";
        string coord_str = "(" + to_string(goal_state.r) + ", " + to_string(goal_state.c) + ")";
        solution.push_back(coord_str);
        Maze::traceBack(goal_state, solution);
        reverse(solution.begin(), solution.end());
        return solution;
//        cout << "yay!" << endl;
//        cout << solution.r << " " << solution.c << " " << solution.orientation << endl;
    }

    void Maze::traceBack(State goal_state, vector<string>& solution) {
        State predecessor = predecessors[Maze::makeKey(goal_state.r, goal_state.c, goal_state.orientation, goal_state.turn)];
//        string coord_str = "(" + to_string(predecessor.r) + ", " + to_string(predecessor.c) + ", " + predecessor.orientation + ", " + predecessor.turn + ")";
        string coord_str = "(" + to_string(predecessor.r) + ", " + to_string(predecessor.c) + ")";
        // cout << coord_str << endl;
        solution.push_back(coord_str);
        if (predecessor.r < 0 || predecessor.r >= h || predecessor.c < 0 || predecessor.c >= w) {
            return;
        }
        traceBack(predecessor, solution);
    }

    Maze::State Maze::bfs() {
        while (!q.empty()) {
            Maze::State s = q.front();
            q.pop();
            int r = s.r;
            int c = s.c;
            string orientation = s.orientation;
            string turn = s.turn;
            if (r < 0 || c < 0 || r >= h || c >= w) {
                continue;
            }
            if (inSet(visited, s)) {
                // cout << "Already visited: " << s << endl;
                continue;
            }
            if (input[r][c] == 'X') {
                continue;
            }
            // cout << s << ", input = " << input[r][c] << endl;
            if (input[r][c] == '*') {
                // cout << "yay" << endl;
                return s;
            }
            else if (input[r][c] == '?') {
                if (orientation == "north") {
                    qpush(Maze::State(r-1, c, "north", "S"));
                    predecessors.insert({Maze::makeKey(r-1, c, "north", "S"), s});
                    qpush(Maze::State(r+1, c, "south", "U"));
                    predecessors.insert({Maze::makeKey(r+1, c, "south", "U"), s});
                    qpush(Maze::State(r, c-1, "west", "L"));
                    predecessors.insert({Maze::makeKey(r, c-1, "west", "L"), s});
                    qpush(Maze::State(r, c+1, "east", "R"));
                    predecessors.insert({Maze::makeKey(r, c+1, "east", "R"), s});
                }
                else if (orientation == "south") {
                    qpush(Maze::State(r-1, c, "north", "U"));
                    predecessors.insert({Maze::makeKey(r-1, c, "north", "U"), s});
                    qpush(Maze::State(r+1, c, "south", "S"));
                    predecessors.insert({Maze::makeKey(r+1, c, "south", "S"), s});
                    qpush(Maze::State(r, c-1, "west", "R"));
                    predecessors.insert({Maze::makeKey(r, c-1, "west", "R"), s});
                    qpush(Maze::State(r, c+1, "east", "L"));
                    predecessors.insert({Maze::makeKey(r, c+1, "east", "L"), s});
                }
                else if (orientation == "west") {
                    qpush(Maze::State(r-1, c, "north", "R"));
                    predecessors.insert({Maze::makeKey(r-1, c, "north", "R"), s});
                    qpush(Maze::State(r+1, c, "south", "L"));
                    predecessors.insert({Maze::makeKey(r+1, c, "south", "L"), s});
                    qpush(Maze::State(r, c-1, "west", "S"));
                    predecessors.insert({Maze::makeKey(r, c-1, "west", "S"), s});
                    qpush(Maze::State(r, c+1, "east", "U"));
                    predecessors.insert({Maze::makeKey(r, c+1, "east", "U"), s});
                }
                else if (orientation == "east") {
                    qpush(Maze::State(r-1, c, "north", "L"));
                    predecessors.insert({Maze::makeKey(r-1, c, "north", "L"), s});
                    qpush(Maze::State(r+1, c, "south", "R"));
                    predecessors.insert({Maze::makeKey(r+1, c, "south", "R"), s});
                    qpush(Maze::State(r, c-1, "west", "U"));
                    predecessors.insert({Maze::makeKey(r, c-1, "west", "U"), s});
                    qpush(Maze::State(r, c+1, "east", "S"));
                    predecessors.insert({Maze::makeKey(r, c+1, "east", "S"), s});
                }
            }
            else if (input[r][c] == 'L') {
                if (orientation == "north") {
                    qpush(Maze::State(r, c-1, "west", "L"));
                    predecessors.insert({Maze::makeKey(r, c-1, "west", "L"), s});
                }
                else if (orientation == "south") {
                    qpush(Maze::State(r, c+1, "east", "L"));
                    predecessors.insert({Maze::makeKey(r, c+1, "east", "L"), s});
                }
                else if (orientation == "east") {
                    qpush(Maze::State(r-1, c, "north", "L"));
                    predecessors.insert({Maze::makeKey(r-1, c, "north", "L"), s});
                }
                else if (orientation == "west") {
                    qpush(Maze::State(r+1, c, "south", "L"));
                    predecessors.insert({Maze::makeKey(r+1, c, "south", "L"), s});
                }
            }
            else if (input[r][c] == 'R') {
                if (orientation == "north") {
                    qpush(Maze::State(r, c+1, "east", "R"));
                    predecessors.insert({Maze::makeKey(r, c+1, "east", "R"), s});
                }
                else if (orientation == "south") {
                    qpush(Maze::State(r, c-1, "west", "R"));
                    predecessors.insert({Maze::makeKey(r, c-1, "west", "R"), s});
                }
                else if (orientation == "east") {
                    qpush(Maze::State(r+1, c, "south", "R"));
                    predecessors.insert({Maze::makeKey(r+1, c, "south", "R"), s});
                }
                else if (orientation == "west") {
                    qpush(Maze::State(r-1, c, "north", "R"));
                    predecessors.insert({Maze::makeKey(r-1, c, "north", "R"), s});
                }
            }
            else if (input[r][c] == 'S') {
                if (orientation == "north") {
                    qpush(Maze::State(r-1, c, "north", "S"));
                    predecessors.insert({Maze::makeKey(r-1, c, "north", "S"), s});
                }
                else if (orientation == "south") {
                    qpush(Maze::State(r+1, c, "south", "S"));
                    predecessors.insert({Maze::makeKey(r+1, c, "south", "S"), s});
                }
                else if (orientation == "east") {
                    qpush(Maze::State(r, c+1, "east", "S"));
                    predecessors.insert({Maze::makeKey(r, c+1, "east", "S"), s});
                }
                else if (orientation == "west") {
                    qpush(Maze::State(r, c-1, "west", "S"));
                    predecessors.insert({Maze::makeKey(r, c-1, "west", "S"), s});
                }
            }
            else if (input[r][c] == 'U') {
                if (orientation == "north") {
                    qpush(Maze::State(r+1, c, "south", "U"));
                    predecessors.insert({Maze::makeKey(r+1, c, "south", "U"), s});
                }
                else if (orientation == "south") {
                    qpush(Maze::State(r-1, c, "north", "U"));
                    predecessors.insert({Maze::makeKey(r-1, c, "north", "U"), s});
                }
                else if (orientation == "east") {
                    qpush(Maze::State(r, c-1, "west", "U"));
                    predecessors.insert({Maze::makeKey(r, c-1, "west", "U"), s});
                }
                else if (orientation == "west") {
                    qpush(Maze::State(r, c+1, "east", "U"));
                    predecessors.insert({Maze::makeKey(r, c+1, "east", "U"), s});
                }
            }

            visited.insert(Maze::makeKey(s.r, s.c, s.orientation, s.turn));
        }
        return Maze::State(-1, -1, "no_solution", "no_solution");
    }

    bool Maze::State::operator==(const Maze::State& rhs) const {
        if (r == rhs.r && c == rhs.c && orientation == rhs.orientation && turn == rhs.turn) {
            return true;
        }
        return false;
    }

    Maze::State::State() {
    }

    Maze::State::State(int r, int c, string orientation, string turn) {
        this->r = r;
        this->c = c;
        this->orientation = orientation;
        this->turn = turn;
    }

    ostream& operator<<(ostream& os, const Maze::State& s) {
        os << "(r = " << s.r << ", c = " << s.c << ", orientation = " << s.orientation << ", turn = " << s.turn <<  ")";
        return os;
    }

    void Maze::qpush(const Maze::State& s) {
        Maze::State s2 = s;
        q.push(s2);
        // cout << "\tPushed: " << s << endl;
    }
}

//namespace std {
//    template<>
//    class std::hash<cs427_527::Maze::State> {
//    public:
//        size_t operator()(const State& s) {
//            size_t str_hash = 0;
//            for (char c : s.orientation) {
//                str_hash += int(c);
//            }
//            return s.r * 37 + s.c * 17 + str_hash * 7;
//        }
//    };
//}



#endif