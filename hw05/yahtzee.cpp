#include "yahtzee.hpp"

#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
using namespace std;

namespace cs427_527
{

    YahtzeeGame::YahtzeeGame(vector<shared_ptr<Rule> > rs, vector<Category> cats)
    {
        this->rules = rs;
        this->categories = cats;
    }

    Scoresheet YahtzeeGame::initialSheet() const {
        return Scoresheet(*this, rules, categories);
    }

    bool YahtzeeGame::isTerminal(const Scoresheet& sheet) const {
        return sheet.isTerminal();
    }
    void YahtzeeGame::scoreRoll(const DiceRoll& roll, string cat, Scoresheet& sheet) const {
        Category& category = sheet.abbrev2Cat(cat);
        const shared_ptr<Rule> rule = category.rule;
        category.update(rule->getScore(roll));
        category.set_to_used();
        for (Category& cat : sheet.bonuses) {
            cat.rule->getScore(roll, sheet);
        }
    }

    Scoresheet::Scoresheet(const YahtzeeGame& g, vector<shared_ptr<Rule> > rs, vector<Category> cats) : game(g) // initialization list allows us to assign references
    {
        this->rules = rs;
        this->categories = cats;
        for (int i = 0; i < cats.size(); i++) {
            abbrev_2_cat[cats[i].abbrev] = i;
//            cout << cats[i].abbrev << " " << cats[i].rule->abbrev << endl;
        }
        for (Category cat : cats) {
            if (cat.is_bonus) {
                bonuses.push_back(cat);
            }
        }
//        for (Category& cat : bonuses) {
//            cout << cat << " " << cat.rule->abbrev << endl;
//        }
    }

    Category& Scoresheet::abbrev2Cat(string abbrev) {
        return categories[abbrev_2_cat.at(abbrev)];
    }

    bool Scoresheet::isTerminal() const
    {
        return unusedCategories().size() == 0;
    }

    void Scoresheet::output(std::ostream& os) const
    {
        int grand_total = 0;
        int fill_length = 4;

//        for (auto& cat : categories)
//        {
//            int padding = 0;
//            if (cat.used || cat.is_bonus) {
//                padding = to_string(cat.score).length();
//                for (int i = 0; i < fill_length - padding - 1; i++) {
//                    os << " ";
//                }
//                os << cat.score << " ";
//            }
//            else {
//                for (int i = 0; i < fill_length - padding; i++) {
//                    os << " ";
//                }
//            }
//            os << cat.full_name << endl;
//
//            grand_total += cat.score;
//        }
//        int padding = to_string(grand_total).length();
//        for (int i = 0; i < fill_length - padding - 1; i++) {
//            os << " ";
//        }
//        os << grand_total << " " << "GRAND TOTAL" << endl;

        for (auto& cat : categories)
        {
            if (cat.used || cat.is_bonus) {
                os << std::setw(fill_length) << cat.score;
                os << " " << cat.full_name << endl;
            }
            else {
                os << "    ";
                os << " " << cat.full_name << endl;
            }
            grand_total += cat.score;
        }
        os << std::setw(fill_length) << grand_total;
        os << " " << "GRAND TOTAL" << endl;

    }

    vector<string> Scoresheet::unusedCategories() const {
        vector<string> unused;
        for (auto& cat : categories) {
            if (!cat.used && !cat.is_bonus) {
                unused.push_back(cat.abbrev);
            }
        }
        return unused;
    }

    std::ostream& operator<<(std::ostream& os, const Scoresheet& sheet)
    {
        sheet.output(os);

        return os;
    }
}
