#include "rule.hpp"

#include "yahtzee.hpp"

#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

namespace cs427_527 {

    Rule::Rule() {

    }

    DiceOfN::DiceOfN(int n) {
        this->n = n;
        abbrev = "DO" + to_string(n);
    }

    int DiceOfN::getScore(const cs427_527::DiceRoll &roll) const {
        return roll.count(n) * n;
    }

    NOfAKind::NOfAKind(int n) {
        this->n = n;
        abbrev = to_string(n) + "OAK";
    }

    FixedScoreRule::FixedScoreRule() {

    }

    int FixedScoreRule::getScore(const cs427_527::DiceRoll &roll) const {
        return score;
    }

    SumScoreRule::SumScoreRule() {
    }

    int SumScoreRule::getScore(const cs427_527::DiceRoll &roll) const {
        return roll.total();
    }

    int NOfAKind::getScore(const cs427_527::DiceRoll &roll) const {
        for (int i = 1; i <= roll.NUM_SIDES; i++) {
            if (roll.count(i) >= n) {
                return roll.total();
            }
        }
        return 0;
    }

    FullHouse::FullHouse(int x1, int x2, int score) {
        this->x1 = x1;
        this->x2 = x2;
        this->score = score;
        abbrev = "FH" + to_string(x1) + "x" + to_string(x2);
    }

    int FullHouse::getScore(const cs427_527::DiceRoll &roll) const {
        for (int i = 1; i <= roll.NUM_SIDES; i++) {
            for (int j = 1; j <= roll.NUM_SIDES; j++) {
                if (i != j && roll.count(i) >= x1 && roll.count(j) >= x2) {
                    return score;
                }
            }
        }
        return 0;
    }

    Straight::Straight(int x, int score) {
        this->x = x;
        this->score = score;
        abbrev = "S" + to_string(x);
    }

    int Straight::getScore(const cs427_527::DiceRoll &roll) const {
        if (x > roll.NUM_DICE) { // x consecutive numbers
            return 0;
        }
        for (int i = 1; i + x <= roll.NUM_SIDES + 1; i++) {
            bool found = true;
            for (int j = i; j < i + x; j++) {
                if (roll.count(j) < 1) {
                    found = false;
                    break;
                }
            }
            if (found) {
                return score;
            }
        }
        return 0;
    }

    Yahtzee::Yahtzee(int score) {
        this->score = score;
        abbrev = "Y";
    }

    int Yahtzee::getScore(const cs427_527::DiceRoll &roll) const {
        if (roll.allSame()) {
            return score;
        }
        return 0;
    }

    Chance::Chance() {
        abbrev = "C";
    }

    BonusRule::BonusRule(int bonus) {
        this->bonus = bonus;
    }

    int BonusRule::getScore(const cs427_527::DiceRoll &roll) const {
        return bonus;
    }

    UpperBonusRule::UpperBonusRule(vector<int> category_range, int threshhold, int own_cat_index, int bonus) {
        this->bonus = bonus;
        this->abbrev = "UBRule";
        this->category_range = category_range;
        this->threshhold = threshhold;
        this->own_cat_index = own_cat_index;
    }

    int UpperBonusRule::getScore(const cs427_527::DiceRoll &roll, cs427_527::Scoresheet &sheet) const {
        int sum = 0;
        for (int cat_index : category_range) {
            sum += sheet.categories.at(cat_index).score;
        }
        if (sum >= threshhold && !sheet.categories.at(own_cat_index).used) {
            sheet.categories.at(own_cat_index).score = bonus;
            sheet.categories.at(own_cat_index).used = true;
        }
        return sum;
    }

    Rule::Rule(const cs427_527::Rule& other) {
        this->name = other.name;
        this->abbrev = other.abbrev;
        this->score = other.score;
    }

    Rule& Rule::operator=(const cs427_527::Rule &other) {
        if (this == &other) {
            return *this;
        }
        this->name = other.name;
        this->abbrev = other.abbrev;
        this->score = other.score;
        return *this;
    }
}