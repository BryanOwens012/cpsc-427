#include "factory.hpp"

#include <memory>
using namespace std;

namespace cs427_527
{

    YahtzeeGame BasicYahtzeeFactory::makeGame() const {
        // Create rules
        vector<shared_ptr<Rule> > rules;
        for (int i = 1; i <= example_dice.NUM_SIDES; i++) {
            rules.push_back(make_shared<DiceOfN>(DiceOfN(i)));
        }
        vector<int> upper_bonus_indexes = {0, 1, 2, 3, 4, 5};
        rules.push_back(make_shared<UpperBonusRule>(UpperBonusRule(upper_bonus_indexes, 63, 8, 35)));
        for (int i = 3; i <= 4; i++) {
            rules.push_back(make_shared<NOfAKind>(NOfAKind(i)));
        }
//        vector<shared_ptr<Rule> > other_rules = { make_shared<UpperBonusRule>(UpperBonusRule()), make_shared<FullHouse>(FullHouse()),
//                                     make_shared<Straight>(Straight(4)), make_shared<Straight>(Straight(5)),
//                                     make_shared<Yahtzee>(Yahtzee()), make_shared<Chance>(Chance()) };
        vector<shared_ptr<Rule> > other_rules;
        other_rules.push_back(make_shared<FullHouse>(FullHouse()));
        other_rules.push_back(make_shared<Straight>(Straight(4, 30)));
        other_rules.push_back(make_shared<Straight>(Straight(5, 40)));
        other_rules.push_back(make_shared<Chance>(Chance()));
        other_rules.push_back(make_shared<Yahtzee>(Yahtzee(50)));
        rules.insert(rules.end(), std::begin(other_rules), std::end(other_rules));

        // Create categories
        vector<Category> categories = { Category("1", "Aces", rules[0]), Category("2", "Deuces", rules[1]),
                                        Category("3", "Treys", rules[2]), Category("4", "Fours", rules[3]),
                                        Category("5", "Fives", rules[4]), Category("6", "Sixes", rules[5]),
                                        Category("UB", "UPPER BONUS", rules[6], true), // is a bonus
                                        Category("3K", "Three of a Kind", rules[7]),
                                        Category("4K", "Four of a Kind", rules[8]),
                                        Category("FH", "Full House", rules[9]),
                                        Category("SS", "Small Straight", rules[10]),
                                        Category("LS", "Large Straight", rules[11]),
                                        Category("C", "Chance", rules[12]),
                                        Category("Y", "Yahtzee", rules[13]) };

        return YahtzeeGame(rules, categories);

    }

    YahtzeeGame MysteryYahtzeeFactory::makeGame() const {
        // Create rules
        vector<shared_ptr<Rule> > rules;
        for (int i = 1; i <= example_dice.NUM_SIDES; i++) {
            rules.push_back(make_shared<DiceOfN>(DiceOfN(i)));
        }
        for (int i = 3; i <= 4; i++) {
            rules.push_back(make_shared<NOfAKind>(NOfAKind(i)));
        }
//        vector<shared_ptr<Rule> > other_rules = { make_shared<UpperBonusRule>(UpperBonusRule()), make_shared<FullHouse>(FullHouse()),
//                                     make_shared<Straight>(Straight(4)), make_shared<Straight>(Straight(5)),
//                                     make_shared<Yahtzee>(Yahtzee()), make_shared<Chance>(Chance()) };
        vector<shared_ptr<Rule> > other_rules;
        vector<int> upper_bonus_indexes = {0, 1, 2, 3, 4, 5};
        other_rules.push_back(make_shared<UpperBonusRule>(UpperBonusRule(upper_bonus_indexes, 63, 35)));
        other_rules.push_back(make_shared<FullHouse>(FullHouse()));
        other_rules.push_back(make_shared<Straight>(Straight(4)));
        other_rules.push_back(make_shared<Straight>(Straight(5)));
        other_rules.push_back(make_shared<Yahtzee>(Yahtzee()));
        other_rules.push_back(make_shared<Chance>(Chance()));
        rules.insert(rules.end(), std::begin(other_rules), std::end(other_rules));

        // Create categories
        vector<Category> categories = { Category("1", "Aces", rules[0]), Category("2", "Deuces", rules[1]),
                                        Category("3", "Treys", rules[2]), Category("4", "Fours", rules[3]),
                                        Category("5", "Fives", rules[4]), Category("6", "Sixes", rules[5]),
                                        Category("3K", "Three of a Kind", rules[6]),
                                        Category("4K", "Four of a Kind", rules[7]),
                                        Category("UB", "UPPER BONUS", rules[8], true), // is a bonus
                                        Category("FH", "Full House", rules[9]),
                                        Category("SS", "Small Straight", rules[10]),
                                        Category("LS", "Large Straight", rules[11]),
                                        Category("Y", "Yahtzee", rules[12]),
                                        Category("C", "Chance", rules[13]) };

        return YahtzeeGame(rules, categories);
    }

//    shared_ptr<IYahtzeePlayer> AdaptivePlayerFactory::makeConservativePlayer() const
//    {
//        return make_shared<YahtzeePlayer>(make_shared<GreedyProgressCategoryStrategy>(),
//                                        make_shared<DynamicLimitRollStrategy>(4, 3));
//    }
//
//    shared_ptr<IYahtzeePlayer> AdaptivePlayerFactory::makeNormalPlayer() const
//    {
//        return make_shared<YahtzeePlayer>(make_shared<GreedyProgressCategoryStrategy>(),
//                                        make_shared<DynamicLimitRollStrategy>(5, 2));
//    }
//
//    shared_ptr<IYahtzeePlayer> AdaptivePlayerFactory::makeAggressivePlayer() const
//    {
//        return make_shared<YahtzeePlayer>(make_shared<GreedyProgressCategoryStrategy>(),
//                                        make_shared<DynamicLimitRollStrategy>(8, 1));
//    }


}
