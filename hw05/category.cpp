#include "category.hpp"

#include <iostream>
using namespace std;

namespace cs427_527 {

    Category::Category(string abbrev, string full_name, shared_ptr<Rule> r, bool is_bonus) : rule(r) {
        this->abbrev = abbrev;
        this->full_name = full_name;
        this->is_bonus = is_bonus;
        this->rule = r;
    }

    void Category::update(int score) {
        this->score = score;
    }

    void Category::set_to_unused() {
        used = false;
    }

    void Category::set_to_used() {
        used = true;
    }

    void Category::output_abbrev(std::ostream& os) const {
        os << abbrev;
    }

    void Category::output_full(std::ostream& os) const {
        os << score << " " << full_name;
    }

    std::ostream& operator<<(std::ostream& os, const Category& cat) {
        cat.output_full(os);
        return os;
    }

    Category::Category(const cs427_527::Category& other) : rule(other.rule) {
        this->abbrev = other.abbrev;
        this->full_name = other.full_name;
        this->used = other.used;
        this->is_bonus = other.is_bonus;
        this->rule = other.rule;
    }

    Category& Category::operator=(const cs427_527::Category &other) {
        if (this == &other) {
            return *this;
        }
        this->abbrev = other.abbrev;
        this->full_name = other.full_name;
        this->used = other.used;
        this->is_bonus = other.is_bonus;
        this->rule = other.rule;
        return *this;
    }

}