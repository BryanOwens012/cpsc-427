#ifndef __FACTORY_HPP__
#define __FACTORY_HPP__
//
#include "yahtzee.hpp"
#include "rule.hpp"
#include "diceroll.hpp"
#include <memory>
using namespace std;

namespace cs427_527
{

    class YahtzeeFactory
    {
    public:
        virtual ~YahtzeeFactory() = default;
        virtual YahtzeeGame makeGame() const = 0;
        DiceRoll example_dice;
    };

    class BasicYahtzeeFactory : public YahtzeeFactory
    {
    public:
        virtual YahtzeeGame makeGame() const override;
    };

    class MysteryYahtzeeFactory : public YahtzeeFactory
    {
    public:
        virtual YahtzeeGame makeGame() const override;
    };

//    class AdaptivePlayerFactory : public PlayerFactory
//    {
//    public:
//        virtual shared_ptr<IYahtzeePlayer> makeAggressivePlayer() const;
//        virtual shared_ptr<IYahtzeePlayer> makeNormalPlayer() const;
//        virtual shared_ptr<IYahtzeePlayer> makeConservativePlayer() const;
//    };

}
#endif