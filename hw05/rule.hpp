#ifndef __RULE_HPP__
#define __RULE_HPP__

#include "diceroll.hpp"
//#include "yahtzee.hpp"

#include <iostream>
#include <vector>
#include <unordered_map>
#include <memory>
using namespace std;

namespace cs427_527 {

    class Scoresheet;

    class Rule {
    public:
        virtual ~Rule() = default;
        virtual int getScore(const DiceRoll &roll) const = 0;
        virtual int getScore(const DiceRoll &roll, Scoresheet &sheet) const {
            return score;
        }
        Rule();

        Rule(const Rule& other);

        Rule& operator=(const Rule &other);

        string name;
        string abbrev;
        int score = 0;
    };

    class FixedScoreRule : public Rule {
    public:
        FixedScoreRule();
        virtual int getScore(const DiceRoll &roll) const override;
    };

    class SumScoreRule : public Rule {
    public:
        SumScoreRule();
        virtual int getScore(const DiceRoll &roll) const override;
    };

    class DiceOfN : public Rule {
        // Acies, Twos, Threes, Fours, Fives, Sixes
    public:
        DiceOfN(int n);
        virtual int getScore(const DiceRoll &roll) const override;

        int n;
    };

    class NOfAKind : public SumScoreRule {
        // E.g., 3 of a kind, 4 of a kind
    public:
        NOfAKind(int n);
        virtual int getScore(const DiceRoll &roll) const override;
        int n;
    };

    class FullHouse : public FixedScoreRule {
        // x1 of one number and x2 of another
    public:
        FullHouse(int x1 = 3, int x2 = 2, int score = 25);
        virtual int getScore(const DiceRoll &roll) const override;
        int x1, x2;
    };

    class Straight : public FixedScoreRule {
        // x sequential dice
    public:
        Straight(int x, int score = 30);
        virtual int getScore(const DiceRoll &roll) const override;
        int x;
    };

    class Yahtzee : public Rule {
        // All five dice the same
    public:
        virtual int getScore(const DiceRoll &roll) const override;
        Yahtzee(int score = 50);
    };

    class Chance : public SumScoreRule {
        // Any combination
    public:
        Chance();
    };

    class BonusRule : public Rule {
    public:
        BonusRule(int bonus = 35);
        virtual ~BonusRule() = default;
        virtual int getScore(const DiceRoll &roll) const override;
        virtual int getScore(const DiceRoll &roll, Scoresheet &sheet) const override {
            return bonus;
        }

        int bonus;
    };

    class UpperBonusRule : public BonusRule {
        // If a player scores a total of 63 or more points in the upper section,
        // a bonus of 35 is added to the upper section score.
    public:
        UpperBonusRule(vector<int> category_range, int threshhold, int own_cat_index, int bonus = 35);
        virtual int getScore(const DiceRoll &roll, Scoresheet &sheet) const override;

        int threshhold;
        int own_cat_index;
        vector<int> category_range;
    };

}

#endif